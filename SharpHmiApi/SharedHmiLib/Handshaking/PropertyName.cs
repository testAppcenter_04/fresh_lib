﻿using System;
using HmiApiLib.Types;

namespace HmiApiLib.Handshaking
{
	public class PropertyName
	{
		public string propertyName { get; set; }


		public PropertyName()
		{
		}

		public void setPropertyName(ComponentPrefix? prefix, FunctionType? type)
		{
            if ((prefix != null) && (type != null))
            {
				propertyName = prefix.ToString() + "." + type.ToString();
            }
		}
	}
}
