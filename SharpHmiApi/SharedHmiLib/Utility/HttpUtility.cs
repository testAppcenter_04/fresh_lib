﻿using System;
using System.IO;
using System.Net;
using HmiApiLib.Manager;

namespace HmiApiLib
{
	public class HttpUtility
	{
		static string splitSubString = "/storage/";
		static int portNumber = 8080;
		public static Stream downloadFile(string fileName)
		{
			if (fileName == null)
			{
				return null;
			}

			string ipAddress = ConnectionManager.Instance.getIpAddress();
			string finalUrl = "http://" + ipAddress + ":" + portNumber + splitSubString + fileName;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(finalUrl);
			request.AutomaticDecompression = DecompressionMethods.GZip;

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			return response.GetResponseStream();
		}

		public static string getStoredFileName(string inputImageName)
		{

			if (inputImageName == null)
			{
				return null;
			}

			string appIconFile = "";

			int index = inputImageName.IndexOf(splitSubString, StringComparison.CurrentCulture);

			if (index != -1)
			{
				appIconFile = inputImageName.Substring(index + splitSubString.Length);
			}
			else
			{
				return null;
			}

			return appIconFile;
		}

		public static int getAppId(string storedFileName)
		{
			int appId = -1;

			if (storedFileName == null)
			{
				return appId;
			}

			int index = storedFileName.IndexOf("_", StringComparison.CurrentCulture);

			if (index != -1)
			{
				appId = int.Parse(storedFileName.Substring(0, index));
			}

			return appId;		}

		public static string getAppStorageDirectory(string storedFileName)
		{
			string appStorageDir = "";

			if (storedFileName == null)
			{
				return appStorageDir;
			}

			int index = storedFileName.IndexOf("/", StringComparison.CurrentCulture);

			if (index != -1)
			{
				appStorageDir = storedFileName.Substring(0, index);
			}

			return appStorageDir;
		}

		public static string getFileName(string storedFileName)
		{
			string fileName = "";

			if (storedFileName == null)
			{
				return fileName;
			}

			int index = storedFileName.IndexOf("/", StringComparison.CurrentCulture);

			if (index != -1)
			{
				fileName = storedFileName.Substring(index + 1);
			}

			return fileName;		}
	}
}
