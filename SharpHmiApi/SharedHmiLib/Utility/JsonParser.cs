﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using HmiApiLib.Base;
using HmiApiLib.Manager;
using HmiApiLib.Types;
using Newtonsoft.Json.Linq;

namespace HmiApiLib.Utility
{
    public sealed class JsonParser
    {
        private static volatile JsonParser instance;
        private static object syncRoot = new Object();
        public static Boolean bRecycled = false;
        private volatile Boolean isHalted = false;
        private const string methodParam = "method";
        private const string resultParam = "result";
        Thread taskThread = null;
        List<String> arr = new List<String>();

        public static JsonParser Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new JsonParser();
                    }
                }
                else
                {
                    bRecycled = true;
                }

                return instance;
            }
        }

        private JsonParser()
        {
        }

        public void startAutoParseJSONFile(Stream inputStream, double? delayTimerInSec)
        {

            if (delayTimerInSec == null) return;

            StreamReader reader = null;

            arr.Clear();

            if (inputStream != null)
            {
                try
                {
                    reader = new StreamReader(inputStream);
                    String line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Length > 0)
                        {
                            arr.Add(line);
                        }
                    }
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (IOException e)
#pragma warning restore CS0168 // Variable is declared but never used
                {

                }
                finally
                {
                    if (reader != null)
                    {
                        try
                        {
                            reader.Close();
                        }
#pragma warning disable CS0168 // Variable is declared but never used
                        catch (IOException e)
#pragma warning restore CS0168 // Variable is declared but never used
                        {

                        }
                    }
                }

                try
                {

                    inputStream.Close();
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (IOException e)
#pragma warning restore CS0168 // Variable is declared but never used
                {

                }
            }

            if (arr.Count > 0)
            {
                if (taskThread != null)
                {
                    stopAutoParseJSONFile();
                }

                taskThread = new Thread(new ParameterizedThreadStart(processJsonRPC));
                taskThread.Start(delayTimerInSec);
                halt(false);
            }
        }

        public void stopAutoParseJSONFile()
        {
            if ((taskThread != null) && (taskThread.IsAlive))
            {
                try
                {
                    halt(true);
                    taskThread.Abort();
                }
#pragma warning disable CS0168 // Variable is declared but never used
#pragma warning disable RECS0022 // A catch clause that catches System.Exception and has an empty body
                catch (Exception ex)
#pragma warning restore RECS0022 // A catch clause that catches System.Exception and has an empty body
#pragma warning restore CS0168 // Variable is declared but never used
                {

                }
            }
			taskThread = null;
        }

        public void processJsonRPC(object timerVal)
        {
            double timerValue = (double)timerVal;

            while (!isHalted)
            {
                JObject level1 = null;
                JObject level2 = null;
                string method = null;
                StringLogMessage myStringLog = null;

                for (int i = 0; i < arr.Count; i++)
                {
                    try
                    {
                        level1 = JObject.Parse(arr[i]);
                    }
#pragma warning disable CS0168 // Variable is declared but never used
#pragma warning disable RECS0022 // A catch clause that catches System.Exception and has an empty body
                    catch (Exception ex)
#pragma warning restore RECS0022 // A catch clause that catches System.Exception and has an empty body
#pragma warning restore CS0168 // Variable is declared but never used
                    {

                    }

                    if ((level1 != null) && (level1[methodParam] != null))
                    {
                        method = level1[methodParam].ToString();
                    }
                    else
                    {

                        //Neither Request / Notification, lets check if its as Response
                        if ((level1 != null) && (level1[resultParam] != null))
                        {
                            try
                            {
                                level2 = JObject.Parse(level1[resultParam].ToString());
                            }
#pragma warning disable CS0168 // Variable is declared but never used
#pragma warning disable RECS0022 // A catch clause that catches System.Exception and has an empty body
                            catch (Exception ex)
#pragma warning restore RECS0022 // A catch clause that catches System.Exception and has an empty body
#pragma warning restore CS0168 // Variable is declared but never used
                            {

                            }
                        }

                        if ((level2 != null) && (level2[methodParam] != null))
                        {
                            method = level2[methodParam].ToString();
                        }
                    }

                    if (method != null)
                    {
                        try
                        {
                            if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.OnVehicleData))
                            {
                                RpcMessage rpcMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData>(arr[i]);
                                ConnectionManager.Instance.sendRpc(rpcMessage);
                            }
                            else
                            {
                                myStringLog = new StringLogMessage(arr[i]);
                                ConnectionManager.Instance.handleInputConsoleLog(myStringLog);
								ConnectionManager.Instance.send(arr[i]);
                            }
                        }
                        catch (Exception ex)
                        {
                            myStringLog = new StringLogMessage("Could not deserialize, json = " + arr[i]);
                            myStringLog.setData("Exception = " + ex);
                            ConnectionManager.Instance.handleInputConsoleLog(myStringLog);

							// If there is only one Json in file and that is corrupted then stop.
							if (arr.Count == 1)
							{
								stopAutoParseJSONFile();
								break;
							}
                        }
                    }
                    else
                    {
                        myStringLog = new StringLogMessage("Corrupted !!! No Method Param present.");
                        myStringLog.setData(arr[i]);
                        ConnectionManager.Instance.handleInputConsoleLog(myStringLog);

                        // If there is only one Json in file and that is corrupted then stop.
                        if(arr.Count == 1)
                        {
                            stopAutoParseJSONFile();
                            break;
                        }
                    }

					try
					{
						Thread.Sleep((int)(timerValue * 1000));
					}
#pragma warning disable CS0168 // Variable is declared but never used
#pragma warning disable RECS0022 // A catch clause that catches System.Exception and has an empty body
                    catch (Exception e)
#pragma warning restore RECS0022 // A catch clause that catches System.Exception and has an empty body
#pragma warning restore CS0168 // Variable is declared but never used
                    {

					}
                }
            }
        }

        /**
        * Method that marks thread as halted.
        */
        public void halt(Boolean flag)
        {
            isHalted = flag;
        }

    }
}
