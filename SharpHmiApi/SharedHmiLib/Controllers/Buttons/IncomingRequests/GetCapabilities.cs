﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Buttons.IncomingRequests
{
	public class GetCapabilities : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.Buttons;
		public GetCapabilities() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetCapabilities.ToString();
		}
	}
}
