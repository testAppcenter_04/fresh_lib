﻿using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.RC.OutgoingResponses
{
	public class GetInteriorVehicleData : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.RC;

		public class InternalData : Base.Result
		{
			public ModuleData moduleData;
            public bool? isSubscribed;
		}

		public void setModuleData(ModuleData moduleData)
		{
			((InternalData)result).moduleData = moduleData;
		}

		public ModuleData getModuleData()
		{
			return ((InternalData)result).moduleData;
		}

		public void setIsSubscribed(bool? isSubscribed)
		{
			((InternalData)result).isSubscribed = isSubscribed;
		}

		public bool? getIsSubscribed()
		{
			return ((InternalData)result).isSubscribed;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetInteriorVehicleData.ToString();
		}

		public GetInteriorVehicleData() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
