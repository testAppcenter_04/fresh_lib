﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.RC.OutgoingResponses
{
	public class ReleaseInteriorVehicleDataConsent : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.RC;

		public class InternalData : Base.Result
		{
            public bool? success;
			public string info;

		}

		public void setSuccess(bool? success)
		{
			((InternalData)result).success = success;
		}

		public bool? getSuccess()
		{
			return ((InternalData)result).success;
		}

		public void setInfo(string info)
		{
			((InternalData)result).info = info;
		}

		public string getInfo()
		{
			return ((InternalData)result).info;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.ReleaseInteriorVehicleDataConsent.ToString();
		}

		public ReleaseInteriorVehicleDataConsent() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
