﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;

namespace HmiApiLib.Controllers.RC.OutGoingNotifications
{
	public class OnRemoteControlSettings : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.RC;
		public Object @params;

		public class InternalData
		{
			public bool? allowed;
            public RCAccessMode? accessMode;
		}

		public void setAllowed(bool? allowed)
		{
			((InternalData)@params).allowed = allowed;
		}

		public bool? getAllowed()
		{
			return ((InternalData)@params).allowed;		}

		public void setAccessMode(RCAccessMode? accessMode)
		{
			((InternalData)@params).accessMode = accessMode;
		}

		public RCAccessMode? getAccessMode()
		{
			return ((InternalData)@params).accessMode;
		}

		public OnRemoteControlSettings() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnRemoteControlSettings.ToString();
			@params = new InternalData();
		}
	}
}