﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;

namespace HmiApiLib.Controllers.RC.OutGoingNotifications
{
	public class OnInteriorVehicleData : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.RC;
		public Object @params;

		public class InternalData
		{
			public ModuleData moduleData;
		}

		public void setModuleData(ModuleData moduleData)
		{
			((InternalData)@params).moduleData = moduleData;
		}

		public ModuleData getModuleData()
		{
			return ((InternalData)@params).moduleData;		}

		public OnInteriorVehicleData() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnInteriorVehicleData.ToString();
			@params = new InternalData();
		}
	}
}