﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.RC.IncomingRequests
{
	public class SetInteriorVehicleData : RpcRequest
	{
        private InterfaceType interfaceType = InterfaceType.RC;
		public class InternalData
		{
            public ModuleData moduleData;
            public int? appID;
		}

		public ModuleData getModuleData()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.moduleData;
		}

		public int? getAppID()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public SetInteriorVehicleData() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.SetInteriorVehicleData.ToString();
		}
	}
}