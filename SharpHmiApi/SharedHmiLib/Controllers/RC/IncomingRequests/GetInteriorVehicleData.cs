﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.RC.IncomingRequests
{
	public class GetInteriorVehicleData : RpcRequest
	{
        private InterfaceType interfaceType = InterfaceType.RC;
		public class InternalData
		{
            public ModuleType? moduleType;
            public bool? subscribe;
		}

		public ModuleType? getModuleType()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.moduleType;
		}

		public bool? getSubscribe()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.subscribe;
		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public GetInteriorVehicleData() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetInteriorVehicleData.ToString();
		}
	}
}