﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.RC.IncomingNotifications
{
	public class OnRCStatus : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.RC;
		public InternalData data = null;
		public Object @params;

		public OnRCStatus() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnRCStatus.ToString();
		}

		public class InternalData
		{
            public int? appID;
			public List<ModuleData> allocatedModules;
			public List<ModuleData> freeModules;
		}

		public int? getAppId()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appID;
		}

		public List<ModuleData> getAllocatedModulesList()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.allocatedModules;
		}

		public List<ModuleData> getFreeModulesList()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.freeModules;
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

	}
}
