﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class Alert : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414


		public class InternalData
		{
			public List<TextFieldStruct> alertStrings;
			public int? duration;
			public List<SoftButton> softButtons;
			public bool? progressIndicator;
			public AlertType alertType;
			public int? appID;
			public int? windowID;

		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getDuration()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.duration;
		}

		public bool? getProgressIndicator()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;
			
			return @params.progressIndicator;
		}

		public AlertType getAlertType()
		{
			if (@params == null)
				setData();

			return @params.alertType;
		}

		public List<TextFieldStruct> getAlertStrings()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.alertStrings;
		}

		public List<SoftButton> getSoftButtons()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.softButtons;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public int? getWindowId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.windowID;
		}
		public Alert() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.Alert.ToString();
		}
	}
}