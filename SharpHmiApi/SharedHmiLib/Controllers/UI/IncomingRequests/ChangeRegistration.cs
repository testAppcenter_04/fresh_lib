﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class ChangeRegistration : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public String appName;
			public String ngnMediaScreenAppName;
			public Language language;
			public List<AppHMIType> appHMIType;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public String getAppName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appName;
		}


		public String getNgnMediaScreenAppName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.ngnMediaScreenAppName;
		}

		public Language getLanguage()
		{
			if (@params == null)
				setData();

			return @params.language;
		}

		public List<AppHMIType> getAppHMIType()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appHMIType;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public ChangeRegistration() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ChangeRegistration.ToString();
		}
	}
}