﻿using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using System.Collections.Generic;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class DeleteWindow : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public int? windowID;
			
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getWindowId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.windowID;
		}


		
		public DeleteWindow() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.DeleteWindow.ToString();
		}
	}

}
