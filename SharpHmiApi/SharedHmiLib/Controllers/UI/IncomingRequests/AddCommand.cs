﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class AddCommand : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public int? cmdID;
			public MenuParams menuParams;
			public Image cmdIcon;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getCmdId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.cmdID;
		}


		public MenuParams getMenuParams()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			MenuParams menu = @params.menuParams;
			return menu;
		}

		public Image getCmdIcon()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			Image icon = @params.cmdIcon;
			return icon;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public AddCommand() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.AddCommand.ToString();
		}
	}
}