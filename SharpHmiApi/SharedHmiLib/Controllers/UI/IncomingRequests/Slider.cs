﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Types;
using System.Collections.Generic;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class Slider : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414


		public class InternalData
		{
			public int? numTicks;
			public int? position;
			public String sliderHeader;
			public List<String> sliderFooter;
			public int? timeout;
			public int? appID;


		}
		public new InternalData @params = null;

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public int? getNumTicks()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.numTicks;
		}

		public int? getPosition()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.position;
		}

		public String getSliderHeader()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.sliderHeader;
		}


		public List<String> getSliderFooter()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.sliderFooter;
		}

		public int? getTimeout()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.timeout;
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public Slider() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.Slider.ToString();
		}
	}
}