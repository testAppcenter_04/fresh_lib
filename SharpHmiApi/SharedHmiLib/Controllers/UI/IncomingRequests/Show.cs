﻿using System;
using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class Show : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public List<TextFieldStruct> showStrings;
			public TextAlignment alignment;
			public Image graphic;
			public Image secondaryGraphic;
			public List<SoftButton> softButtons;
			public List<string> customPresets;
			public int? appID;


		}
		public new InternalData @params = null;

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public TextAlignment getTextAlignment()
		{
			if (@params == null)
				setData();

			return @params.alignment;
		}

		public Image getGraphic()
		{
			if (@params == null)
				setData();

			if (@params == null)
				return null;

			return @params.graphic;
		}

		public Image getSecondaryGraphic()
		{
			if (@params == null)
				setData();

			if (@params == null)
				return null;

			return @params.secondaryGraphic;
		}

		public List<TextFieldStruct> getShowStrings()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.showStrings;
		}


		public List<SoftButton> getSoftButtons()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.softButtons;
		}

		public List<string> getCustomPresets()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.customPresets;
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public Show() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.Show.ToString();
		}
	}
}
