﻿using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using System.Collections.Generic;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class CreateWindow : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public int? windowID;
			public int? duplicateUpdatesFromWindowID;
			public WindowType? type;
			public string windowName;
			public string associatedServiceType;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getWindowId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.windowID;
		}


		public int? getDuplicateUpdatesFromWindowID()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.duplicateUpdatesFromWindowID;
		}

		public string getWindowName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.windowName;
		}

		public string getAssociatedServiceType()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.associatedServiceType;
		}
		public WindowType? getType()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.type;
		}
		public CreateWindow() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.CreateWindow.ToString();
		}
	}

}
