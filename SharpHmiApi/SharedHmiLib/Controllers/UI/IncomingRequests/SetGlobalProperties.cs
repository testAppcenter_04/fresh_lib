﻿using System;
using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using System.Collections.Generic;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class SetGlobalProperties : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public String vrHelpTitle;
			public List<VrHelpItem> vrHelp;
			public String menuTitle;
			public Image menuIcon;
			public KeyboardProperties keyboardProperties;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public String getVrHelpTitle()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.vrHelpTitle;
		}


		public List<VrHelpItem> getVrHelp()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.vrHelp;
		}

		public String getMenuTitle()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.menuTitle;
		}

		public Image getMenuIcon()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.menuIcon;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public KeyboardProperties getKeyboardProperties()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.keyboardProperties;
		}

		public SetGlobalProperties() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.SetGlobalProperties.ToString();
		}
	}
}