﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutgoingResponses
{
	public class Slider : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.UI;

		public class InternalData : Base.Result
		{
			public int? sliderPosition;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.Slider.ToString();
		}

		public void setSliderPosition(int? sliderPosition)
		{
			((InternalData)result).sliderPosition = sliderPosition;
		}

		public int? getSliderPosition()
		{
			return ((InternalData)result).sliderPosition;		}

		public Slider() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}