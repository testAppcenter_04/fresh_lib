﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutgoingResponses
{
	public class PerformInteraction : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.UI;

		public class InternalData : Base.Result
		{
			public int? choiceID;
			public String manualTextEntry;
		}

		public void setChoiceID(int? value)
		{
			((InternalData)result).choiceID = value;
		}

		public void setManualTextEntry(String value)
		{
			((InternalData)result).manualTextEntry = value;
		}

		public int? getChoiceID()
		{
			return ((InternalData)result).choiceID;
		}

		public String getManualTextEntry()
		{
			return ((InternalData)result).manualTextEntry;		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.PerformInteraction.ToString();
		}

		public PerformInteraction() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
