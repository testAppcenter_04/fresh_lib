﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutgoingResponses
{
	public class SetDisplayLayout : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.UI;

		public class InternalData : Base.Result
		{
			public DisplayCapabilities displayCapabilities;
			public List<ButtonCapabilities> buttonCapabilities;
			public List<SoftButtonCapabilities> softButtonCapabilities;
			public PresetBankCapabilities presetBankCapabilities;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.SetDisplayLayout.ToString();
		}

		public void setDisplayCapabilities(DisplayCapabilities displayCapabilities)
		{
			((InternalData)result).displayCapabilities = displayCapabilities;
		}

		public void setButtonCapabilities(List<ButtonCapabilities> buttonCapabilities)
		{
			((InternalData)result).buttonCapabilities = buttonCapabilities;
		}

		public void setSoftButtonCapabilities(List<SoftButtonCapabilities> softButtonCapabilities)
		{
			((InternalData)result).softButtonCapabilities = softButtonCapabilities;
		}

		public void setPresetBankCapabilities(PresetBankCapabilities presetBankCapabilities)
		{
			((InternalData)result).presetBankCapabilities = presetBankCapabilities;
		}

		public DisplayCapabilities getDisplayCapabilities()
		{
			return ((InternalData)result).displayCapabilities;
		}

		public List<ButtonCapabilities> getButtonCapabilities()
		{
			return ((InternalData)result).buttonCapabilities;
		}

		public List<SoftButtonCapabilities> getSoftButtonCapabilities()
		{
			return ((InternalData)result).softButtonCapabilities;
		}

		public PresetBankCapabilities getPresetBankCapabilities()
		{
			return ((InternalData)result).presetBankCapabilities;
		}

		public SetDisplayLayout() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}