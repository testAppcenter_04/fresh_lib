﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.OutgoingResponses
{
	public class SetGlobalProperties : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.UI;

		public class InternalData : Base.Result
		{

		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.SetGlobalProperties.ToString();
		}

		public SetGlobalProperties() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}