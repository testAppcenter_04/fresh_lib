﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutGoingNotifications
{
	public class OnKeyboardInput : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
			public KeyboardEvent? @event;
			public String data;
		}

		public void setKeyboardEvent(KeyboardEvent? @event)
		{
			((InternalData)@params).@event = @event;
		}

		public void setData(String data)
		{
			((InternalData)@params).data = data;
		}

		public KeyboardEvent? getKeyboardEvent()
		{
			return ((InternalData)@params).@event;
		}

		public String getData()
		{
			return ((InternalData)@params).data;		}

		public OnKeyboardInput() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnKeyboardInput.ToString();
			@params = new InternalData();
		}
	}
}