﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutGoingNotifications
{
	public class OnSystemContext : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
			public int? appID;
			public int? windowId;
			public SystemContext? systemContext;
		}

		public void setSystemContext(SystemContext context)
		{
			((InternalData)@params).systemContext = context;
		}

		public void setWindowId(int? id)
		{
			((InternalData)@params).windowId = id;
		}
		public void setAppId(int? id)
		{
			((InternalData)@params).appID = id;
		}

		public SystemContext? getSystemContext()
		{
			return ((InternalData)@params).systemContext;
		}
		public int? getWindowId()
		{
			return ((InternalData)@params).windowId;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;		}

		public OnSystemContext() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnSystemContext.ToString();
			@params = new InternalData();
		}
	}
}
