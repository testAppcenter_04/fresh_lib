﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutGoingNotifications
{
	public class OnDriverDistraction : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
			public DriverDistractionState? state;
		}

		public void setDriverDistractionState(DriverDistractionState? state)
		{
			((InternalData)@params).state = state;
		}

		public DriverDistractionState? getDriverDistractionState()
		{
			return ((InternalData)@params).state;		}

		public OnDriverDistraction() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnDriverDistraction.ToString();
			@params = new InternalData();
		}
	}
}