﻿using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Structs;
using System.Collections.Generic;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.VR.IncomingRequests
{
	public class PerformInteraction : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.VR;
#pragma warning restore 0414

		public class InternalData
		{
			public List<TTSChunk> helpPrompt;
			public List<TTSChunk> initialPrompt;
			public List<TTSChunk> timeoutPrompt;
			public int? timeout;
			public List<int> grammarID;
			public int? appID;

		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getTimeOut()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.timeout;
		}

		public List<TTSChunk> getHelpPrompt()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.helpPrompt;
		}

		public List<TTSChunk> getInitialPrompt()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.initialPrompt;
		}

		public List<TTSChunk> getTimeoutPrompt()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.timeoutPrompt;
		}

		public List<int> getGrammarID()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.grammarID;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public PerformInteraction() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.PerformInteraction.ToString();
		}
	}
}