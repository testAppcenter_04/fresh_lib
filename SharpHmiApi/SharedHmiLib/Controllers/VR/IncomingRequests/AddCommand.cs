﻿using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.VR.IncomingRequests
{
	public class AddCommand : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.VR;
#pragma warning restore 0414

		public class InternalData
		{
			public int? cmdID;
			public List<string> vrCommands;
			public VRCommandType type;
			public int? grammarID;
			public int? appID;

		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getCmdId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.cmdID;
		}

		public List<string> getVrCommands()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.vrCommands;
		}

		public VRCommandType getType()
		{
			if (@params == null)
				setData();

			return @params.type;
		}

		public int? getGrammarId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.grammarID;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public AddCommand() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.AddCommand.ToString();
		}
	}
}