﻿using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.VR.IncomingRequests
{
	public class DeleteCommand : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.VR;
#pragma warning restore 0414

		public class InternalData
		{
			public int? cmdID;
			public VRCommandType type;
			public int? grammarID;
			public int? appID;

		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public VRCommandType getVrCommandType()
		{
			if (@params == null)
				setData();

			return @params.type;
		}


		public int? getCmdId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.cmdID;
		}

		public int? getGrammarId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.grammarID;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public DeleteCommand() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.DeleteCommand.ToString();
		}
	}
}
