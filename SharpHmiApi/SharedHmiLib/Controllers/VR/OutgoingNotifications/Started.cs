﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.VR.OutGoingNotifications
{
	public class Started : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.VR;

		public Started() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.Started.ToString();
		}
	}
}
