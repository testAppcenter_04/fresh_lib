﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VR.OutgoingResponses
{
	public class GetCapabilities : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VR;

		public class InternalData : Base.Result
		{
			public List<VrCapabilities> vrCapabilities;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetCapabilities.ToString();
		}

		public void setVrCapabilities(List<VrCapabilities> vrCapabilities)
		{
			((InternalData)result).vrCapabilities = vrCapabilities;
		}

		public List<VrCapabilities> getVrCapabilities()
		{
			return ((InternalData)result).vrCapabilities;		}

		public GetCapabilities() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}