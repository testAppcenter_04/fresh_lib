﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VR.OutgoingResponses
{
	public class GetLanguage : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VR;

		public class InternalData : Base.Result
		{
			public Language? language;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetLanguage.ToString();
		}

		public void setLanguage(Language? language)
		{
			((InternalData)result).language = language;
		}

		public Language? getLanguage()
		{
			return ((InternalData)result).language;		}

		public GetLanguage() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
