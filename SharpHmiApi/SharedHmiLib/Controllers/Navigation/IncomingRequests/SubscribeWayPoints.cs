﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingRequests
{
	public class SubscribeWayPoints : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.Navigation;
#pragma warning restore 0414

		public SubscribeWayPoints() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.SubscribeWayPoints.ToString();
		}
	}
}