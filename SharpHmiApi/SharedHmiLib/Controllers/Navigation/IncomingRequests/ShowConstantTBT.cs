﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using System.Collections.Generic;
using System;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingRequests
{
	public class ShowConstantTBT : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.Navigation;
		public class InternalData
		{
			public List<TextFieldStruct> navigationTexts;
			public Image turnIcon;
			public Image nextTurnIcon;
			public float? distanceToManeuver;
			public float? distanceToManeuverScale;
			public bool? maneuverComplete;
			public List<SoftButton> softButtons;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public List<TextFieldStruct> getNavigationTexts()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.navigationTexts;
		}

		public Image getTurnIcon()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.turnIcon;
		}

		public Image getNextTurnIcon()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.nextTurnIcon;
		}

		public float? getDistanceToManeuver()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.distanceToManeuver;
		}

		public float? getDistanceToManeuverScale()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.distanceToManeuverScale;
		}

		public bool? getManeuverComplete()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.maneuverComplete;
		}

		public List<SoftButton> getSoftButtons()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.softButtons;
		}

		public ShowConstantTBT() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ShowConstantTBT.ToString();
		}
	}
}