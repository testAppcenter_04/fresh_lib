﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using System.Collections.Generic;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingRequests
{
	public class UpdateTurnList : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.Navigation;
		public class InternalData
		{
			public List<Turn> turnList;
			public List<SoftButton> softButtons;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public List<Turn> getTurnList()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.turnList;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public List<SoftButton> getSoftButtons()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.softButtons;
		}

		public UpdateTurnList() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.UpdateTurnList.ToString();
		}
	}
}