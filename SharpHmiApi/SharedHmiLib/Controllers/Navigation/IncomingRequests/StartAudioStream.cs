﻿using HmiApiLib.Base;
using System;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingRequests
{
	public class StartAudioStream : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.Navigation;
		public class InternalData
		{
			public String url;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public String getUrl()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;
			return @params.url;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public StartAudioStream() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.StartAudioStream.ToString();
		}
	}
}