﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingRequests
{
	public class UnsubscribeWayPoints : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.Navigation;
#pragma warning restore 0414

		public UnsubscribeWayPoints() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.UnsubscribeWayPoints.ToString();
		}
	}
}