﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications
{
	public class OnVehicleData : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;
		public Object @params;

		public class InternalData
		{
			public GPSData gps;
			public float? speed;
			public int? rpm;
			public float? fuelLevel;
			public ComponentVolumeStatus? fuelLevel_State;
			public float? instantFuelConsumption;
			public float? externalTemperature;
			public String vin;
			public PRNDL? prndl;
			public TireStatus tirePressure;
			public int? odometer;
			public BeltStatus beltStatus;
			public BodyInformation bodyInformation;
			public DeviceStatus deviceStatus;
			public VehicleDataEventStatus? driverBraking;
			public WiperStatus? wiperStatus;
			public HeadLampStatus headLampStatus;
			public float? engineTorque;
			public float? accPedalPosition;
			public float? steeringWheelAngle;
			public ECallInfo eCallInfo;
			public AirbagStatus airbagStatus;
			public EmergencyEvent emergencyEvent;
			public ClusterModeStatus clusterModes;
			public MyKey myKey;
            public ElectronicParkBrakeStatus? electronicParkBrakeStatus;
            public float? engineOilLife;
            public List<FuelRange> fuelRange;
            public TurnSignal? turnSignal;
		}

		public void setGps(GPSData gps)
		{
			((InternalData)@params).gps = gps;
		}

		public GPSData getGps()
		{
			return ((InternalData)@params).gps;
		}

		public void setSpeed(float? speed)
		{
			((InternalData)@params).speed = speed;
		}

		public float? getSpeed()
		{
            return ((InternalData)@params).speed;
		}

		public void setRpm(int? rpm)
		{
			((InternalData)@params).rpm = rpm;
		}

		public int? getRpm()
		{
			return ((InternalData)@params).rpm;
		}

		public void setFuelLevel(float? fuelLevel)
		{
			((InternalData)@params).fuelLevel = fuelLevel;
		}

        public float? getFuelLevel()
        {
            return ((InternalData)@params).fuelLevel;
        }

		public void setFuelLevel_State(ComponentVolumeStatus? fuelLevel_State)
		{
			((InternalData)@params).fuelLevel_State = fuelLevel_State;
		}

		public ComponentVolumeStatus? getFuelLevel_State()
		{
			return ((InternalData)@params).fuelLevel_State;
		}

		public void setInstantFuelConsumption(float? instantFuelConsumption)
		{
			((InternalData)@params).instantFuelConsumption = instantFuelConsumption;
		}

		public float? getInstantFuelConsumption()
		{
			return ((InternalData)@params).instantFuelConsumption;
		}

		public void setExternalTemperature(float? externalTemperature)
		{
			((InternalData)@params).externalTemperature = externalTemperature;
		}

		public float? getExternalTemperature()
		{
			return ((InternalData)@params).externalTemperature;
		}

		public void setVin(string vin)
		{
			((InternalData)@params).vin = vin;
		}

		public string getVin()
		{
			return ((InternalData)@params).vin;
		}

		public void setPrndl(PRNDL? prndl)
		{
			((InternalData)@params).prndl = prndl;
		}

		public PRNDL? getPrndl()
		{
			return ((InternalData)@params).prndl;
		}

		public void setTirePressure(TireStatus tirePressure)
		{
			((InternalData)@params).tirePressure = tirePressure;
		}

		public TireStatus getTirePressure()
		{
			return ((InternalData)@params).tirePressure;
		}

		public void setOdometer(int? odometer)
		{
			((InternalData)@params).odometer = odometer;
		}

		public int? getOdometer()
		{
			return ((InternalData)@params).odometer;
		}

		public void setBeltStatus(BeltStatus beltStatus)
		{
			((InternalData)@params).beltStatus = beltStatus;
		}

		public BeltStatus getBeltStatus()
		{
			return ((InternalData)@params).beltStatus;
		}

		public void setBodyInformation(BodyInformation bodyInformation)
		{
			((InternalData)@params).bodyInformation = bodyInformation;
		}

		public BodyInformation getBodyInformation()
		{
			return ((InternalData)@params).bodyInformation;
		}

		public void setDeviceStatus(DeviceStatus deviceStatus)
		{
			((InternalData)@params).deviceStatus = deviceStatus;
		}

		public DeviceStatus getDeviceStatus()
		{
			return ((InternalData)@params).deviceStatus;
		}

		public void setDriverBraking(VehicleDataEventStatus? driverBraking)
		{
			((InternalData)@params).driverBraking = driverBraking;
		}

		public VehicleDataEventStatus? getDriverBraking()
		{
			return ((InternalData)@params).driverBraking;
		}

		public void setWiperStatus(WiperStatus? wiperStatus)
		{
			((InternalData)@params).wiperStatus = wiperStatus;
		}

		public WiperStatus? getWiperStatus()
		{
			return ((InternalData)@params).wiperStatus;
		}

		public void setHeadLampStatus(HeadLampStatus headLampStatus)
		{
			((InternalData)@params).headLampStatus = headLampStatus;
		}

		public HeadLampStatus getHeadLampStatus()
		{
			return ((InternalData)@params).headLampStatus;
		}

		public void setEngineTorque(float? engineTorque)
		{
			((InternalData)@params).engineTorque = engineTorque;
		}

		public float? getEngineTorque()
		{
			return ((InternalData)@params).engineTorque;
		}

		public void setAccPedalPosition(float? accPedalPosition)
		{
			((InternalData)@params).accPedalPosition = accPedalPosition;
		}

		public float? getAccPedalPosition()
		{
			return ((InternalData)@params).accPedalPosition;
		}

		public void setSteeringWheelAngle(float? steeringWheelAngle)
		{
			((InternalData)@params).steeringWheelAngle = steeringWheelAngle;
		}

		public float? getSteeringWheelAngle()
		{
			return ((InternalData)@params).steeringWheelAngle;
		}

		public void setECallInfo(ECallInfo eCallInfo)
		{
			((InternalData)@params).eCallInfo = eCallInfo;
		}

		public ECallInfo getECallInfo()
		{
			return ((InternalData)@params).eCallInfo;
		}

		public void setAirbagStatus(AirbagStatus airbagStatus)
		{
			((InternalData)@params).airbagStatus = airbagStatus;
		}

		public AirbagStatus getAirbagStatus()
		{
			return ((InternalData)@params).airbagStatus;
		}

		public void setEmergencyEvent(EmergencyEvent emergencyEvent)
		{
			((InternalData)@params).emergencyEvent = emergencyEvent;
		}

		public EmergencyEvent getEmergencyEvent()
		{
			return ((InternalData)@params).emergencyEvent;
		}

		public void setClusterModes(ClusterModeStatus clusterModes)
		{
			((InternalData)@params).clusterModes = clusterModes;
		}

		public ClusterModeStatus getClusterModes()
		{
			return ((InternalData)@params).clusterModes;
		}

		public void setMyKey(MyKey myKey)
		{
			((InternalData)@params).myKey = myKey;
		}

		public MyKey getMyKey()
		{
			return ((InternalData)@params).myKey;
		}

		public void setElectronicParkBrakeStatus(ElectronicParkBrakeStatus? electronicParkBrakeStatus)
		{
			((InternalData)@params).electronicParkBrakeStatus = electronicParkBrakeStatus;
		}

		public void setTurnSignal(TurnSignal? turnSignal)
		{
			((InternalData)@params).turnSignal = turnSignal;
		}

		public ElectronicParkBrakeStatus? getElectronicParkBrakeStatus()
		{
			return ((InternalData)@params).electronicParkBrakeStatus;
		}

		public TurnSignal? getTurnSignal()
		{
			return ((InternalData)@params).turnSignal;
		}

        public void setEngineOilLife(float? engineOilLife)
        {
            ((InternalData)@params).engineOilLife = engineOilLife;
        }

        public float? getEngineOilLife()
        {
            return ((InternalData)@params).engineOilLife;
        }

        public void setFuelRange(List<FuelRange> fuelRange)
        {
            ((InternalData)@params).fuelRange = fuelRange;
        }

        public List<FuelRange> getFuelRange()
        {
            return ((InternalData)@params).fuelRange;
        }

		public OnVehicleData() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnVehicleData.ToString();
            @params = new InternalData();
		}
	}
}