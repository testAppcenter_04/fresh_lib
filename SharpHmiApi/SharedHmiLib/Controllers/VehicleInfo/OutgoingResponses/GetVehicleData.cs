﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.OutgoingResponses
{
	public class GetVehicleData : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;

		public class InternalData : Base.Result
		{
			public GPSData gps;
			public float? speed;
			public int? rpm;
			public float? fuelLevel;
			public ComponentVolumeStatus? fuelLevel_State;
			public float? instantFuelConsumption;
			public float? externalTemperature;
			public string vin;
			public PRNDL? prndl;
			public TireStatus tirePressure;
			public int? odometer;
			public BeltStatus beltStatus;
			public BodyInformation bodyInformation;
			public DeviceStatus deviceStatus;
			public VehicleDataEventStatus? driverBraking;
			public WiperStatus? wiperStatus;
			public HeadLampStatus headLampStatus;
			public float? engineTorque;
			public float? accPedalPosition;
			public float? steeringWheelAngle;
			public ECallInfo eCallInfo;
			public AirbagStatus airbagStatus;
			public EmergencyEvent emergencyEvent;
			public ClusterModeStatus clusterModes;
			public MyKey myKey;
            public ElectronicParkBrakeStatus? electronicParkBrakeStatus;
            public float? engineOilLife;
            public List<FuelRange> fuelRange;
			public TurnSignal? turnSignal;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetVehicleData.ToString();
		}

		public void setGps(GPSData gps)
		{
			((InternalData)result).gps = gps;
		}

		public void setSpeed(float? speed)
		{
			((InternalData)result).speed = speed;
		}

		public void setRpm(int? rpm)
		{
			((InternalData)result).rpm = rpm;
		}

		public void setFuelLevel(float? fuelLevel)
		{
			((InternalData)result).fuelLevel = fuelLevel;
		}

		public void setFuelLevel_State(ComponentVolumeStatus? fuelLevel_State)
		{
			((InternalData)result).fuelLevel_State = fuelLevel_State;
		}

		public void setInstantFuelConsumption(float? instantFuelConsumption)
		{
			((InternalData)result).instantFuelConsumption = instantFuelConsumption;
		}

		public void setExternalTemperature(float? externalTemperature)
		{
			((InternalData)result).externalTemperature = externalTemperature;
		}

		public void setVin(String vin)
		{
			((InternalData)result).vin = vin;
		}

		public void setPrndl(PRNDL? prndl)
		{
			((InternalData)result).prndl = prndl;
		}

		public void setTirePressure(TireStatus tirePressure)
		{
			((InternalData)result).tirePressure = tirePressure;
		}

		public void setOdometer(int? odometer)
		{
			((InternalData)result).odometer = odometer;
		}

		public void setBeltStatus(BeltStatus beltStatus)
		{
			((InternalData)result).beltStatus = beltStatus;
		}

		public void setBodyInformation(BodyInformation bodyInformation)
		{
			((InternalData)result).bodyInformation = bodyInformation;
		}

		public void setDeviceStatus(DeviceStatus deviceStatus)
		{
			((InternalData)result).deviceStatus = deviceStatus;
		}

		public void setDriverBraking(VehicleDataEventStatus? driverBraking)
		{
			((InternalData)result).driverBraking = driverBraking;
		}

		public void setWiperStatus(WiperStatus? wiperStatus)
		{
			((InternalData)result).wiperStatus = wiperStatus;
		}

		public void setHeadLampStatus(HeadLampStatus headLampStatus)
		{
			((InternalData)result).headLampStatus = headLampStatus;
		}

		public void setEngineTorque(float? engineTorque)
		{
			((InternalData)result).engineTorque = engineTorque;
		}

		public void setAccPedalPosition(float? accPedalPosition)
		{
			((InternalData)result).accPedalPosition = accPedalPosition;
		}

		public void setSteeringWheelAngle(float? steeringWheelAngle)
		{
			((InternalData)result).steeringWheelAngle = steeringWheelAngle;
		}

		public void setECallInfo(ECallInfo eCallInfo)
		{
			((InternalData)result).eCallInfo = eCallInfo;
		}

		public void setAirbagStatus(AirbagStatus airbagStatus)
		{
			((InternalData)result).airbagStatus = airbagStatus;
		}

		public void setEmergencyEvent(EmergencyEvent emergencyEvent)
		{
			((InternalData)result).emergencyEvent = emergencyEvent;
		}

		public void setClusterModes(ClusterModeStatus clusterModes)
		{
			((InternalData)result).clusterModes = clusterModes;
		}

		public void setMyKey(MyKey myKey)
		{
			((InternalData)result).myKey = myKey;
		}

		public void setElectronicParkBrakeStatus(ElectronicParkBrakeStatus? electronicParkBrakeStatus)
		{
			((InternalData)result).electronicParkBrakeStatus = electronicParkBrakeStatus;
		}

		public void setTurnSignal(TurnSignal? turnSignal)
		{
			((InternalData)result).turnSignal = turnSignal;
		}

        public void setEngineOilLife(float? engineOilLife)
        {
            ((InternalData)result).engineOilLife = engineOilLife;
        }

        public void setFuelRange(List<FuelRange> fuelRange)
        {
            ((InternalData)result).fuelRange = fuelRange;
        }

		public GPSData getGps()
		{
			return ((InternalData)result).gps;
		}

		public float? getSpeed()
		{
			return ((InternalData)result).speed;
		}

		public int? getRpm()
		{
			return ((InternalData)result).rpm;
		}

		public float? getFuelLevel()
		{
			return ((InternalData)result).fuelLevel;
		}

		public ComponentVolumeStatus? getFuelLevel_State()
		{
			return ((InternalData)result).fuelLevel_State;
		}

		public float? getInstantFuelConsumption()
		{
			return ((InternalData)result).instantFuelConsumption;
		}

		public float? getExternalTemperature()
		{
			return ((InternalData)result).externalTemperature;
		}

		public String getVin()
		{
			return ((InternalData)result).vin;
		}

		public PRNDL? getPrndl()
		{
			return ((InternalData)result).prndl;
		}

		public TireStatus getTirePressure()
		{
			return ((InternalData)result).tirePressure;
		}

		public int? getOdometer()
		{
			return ((InternalData)result).odometer;
		}

		public BeltStatus getBeltStatus()
		{
			return ((InternalData)result).beltStatus;
		}

		public BodyInformation getBodyInformation()
		{
			return ((InternalData)result).bodyInformation;
		}

		public DeviceStatus getDeviceStatus()
		{
			return ((InternalData)result).deviceStatus;
		}

		public VehicleDataEventStatus? getDriverBraking()
		{
			return ((InternalData)result).driverBraking;
		}

		public WiperStatus? getWiperStatus()
		{
			return ((InternalData)result).wiperStatus;
		}

		public HeadLampStatus getHeadLampStatus()
		{
			return ((InternalData)result).headLampStatus;
		}

		public float? getEngineTorque()
		{
			return ((InternalData)result).engineTorque;
		}

		public float? getAccPedalPosition()
		{
			return ((InternalData)result).accPedalPosition;
		}

		public float? getSteeringWheelAngle()
		{
			return ((InternalData)result).steeringWheelAngle;
		}

		public ECallInfo getECallInfo()
		{
			return ((InternalData)result).eCallInfo;
		}

		public AirbagStatus getAirbagStatus()
		{
			return ((InternalData)result).airbagStatus;
		}

		public EmergencyEvent getEmergencyEvent()
		{
			return ((InternalData)result).emergencyEvent;
		}

		public ClusterModeStatus getClusterModes()
		{
			return ((InternalData)result).clusterModes;
		}

		public MyKey getMyKey()
		{
			return ((InternalData)result).myKey;
		}

		public ElectronicParkBrakeStatus? getElectronicParkBrakeStatus()
		{
			return ((InternalData)result).electronicParkBrakeStatus;
		}

		public TurnSignal? getTurnSignal()
		{
			return ((InternalData)result).turnSignal;
		}

        public float? getEngineOilLife()
        {
            return ((InternalData)result).engineOilLife;
        }

        public List<FuelRange> getFuelRange()
        {
            return ((InternalData)result).fuelRange;
        }

		public GetVehicleData() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}