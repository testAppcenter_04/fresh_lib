﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.IncomingRequests
{
	public class GetVehicleType : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.VehicleInfo;
#pragma warning restore 0414

		public GetVehicleType() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetVehicleType.ToString();
		}
	}
}