﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.IncomingRequests
{
	public class ReadDID : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;
		public class InternalData
		{
			public int? ecuName;
			public List<int> didLocation;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getEcuName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.ecuName;
		}


		public List<int> getDidLocation()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.didLocation;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public ReadDID() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ReadDID.ToString();
		}
	}
}