﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnStartDeviceDiscovery : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public OnStartDeviceDiscovery() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnStartDeviceDiscovery.ToString();
		}
	}
}