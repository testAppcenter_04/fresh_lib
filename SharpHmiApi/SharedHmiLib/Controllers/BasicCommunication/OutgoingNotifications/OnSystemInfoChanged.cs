﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnSystemInfoChanged : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;

		public class InternalData
		{
			public Language? language;
		}

		public void setLanguage(Language? language)
		{
			((InternalData)@params).language = language;
		}

		public Language? getLanguage()
		{
			return ((InternalData)@params).language;
		}

		public OnSystemInfoChanged() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnSystemInfoChanged.ToString();
			@params = new InternalData();
		}
	}
}