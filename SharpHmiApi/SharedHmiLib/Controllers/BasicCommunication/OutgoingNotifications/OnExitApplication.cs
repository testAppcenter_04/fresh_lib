﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
public class OnExitApplication : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public InternalData data = null;
		public Object @params;

		public OnExitApplication() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnExitApplication.ToString();
            @params = new InternalData();
		}

		public class InternalData
		{
			public ApplicationExitReason? reason;
			public int? appID;
		}

		public void setApplicationExitReason(ApplicationExitReason? reason)
		{
			((InternalData)@params).reason = reason;
		}

		public void setAppId(int? id)
		{
			((InternalData)@params).appID = id;
		}

		public ApplicationExitReason? getApplicationExitReason()
		{
			return ((InternalData)@params).reason;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;		}
	}
}
