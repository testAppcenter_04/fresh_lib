﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnEmergencyEvent : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;
		public class InternalData
		{
			public bool? enabled;
		}

		public void setEnabled(bool? enabled)
		{
			((InternalData)@params).enabled = enabled;
		}

		public bool? getEnabled()
		{
			return ((InternalData)@params).enabled;
		}

		public OnEmergencyEvent() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnEmergencyEvent.ToString();
			@params = new InternalData();
		}
	}
}