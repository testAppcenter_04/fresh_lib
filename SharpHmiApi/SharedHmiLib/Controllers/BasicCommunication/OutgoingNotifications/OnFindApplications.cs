﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnFindApplications : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;

		public class InternalData
		{
			public DeviceInfo deviceInfo;
		}

		public void setDeviceInfo(DeviceInfo deviceInfo)
		{
			((InternalData)@params).deviceInfo = deviceInfo;
		}

		public DeviceInfo getDeviceInfo()
		{
			return ((InternalData)@params).deviceInfo;
		}

		public OnFindApplications() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnFindApplications.ToString();
			@params = new InternalData();
		}
	}
}