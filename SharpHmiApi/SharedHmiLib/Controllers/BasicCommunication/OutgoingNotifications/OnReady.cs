﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnReady : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public OnReady() : base(RpcMessageFlow.OUTGOING)
		{
			method =  interfaceType.ToString() + "." + FunctionType.OnReady.ToString();
		}
	}
}
