﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnEventChanged : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;

		public class InternalData
		{
			public EventTypes eventName;
			public bool? isActive;
		}

		public void setEventName(EventTypes eventName)
		{
			((InternalData)@params).eventName = eventName;
		}

		public void setActive(bool? isActive)
		{
			((InternalData)@params).isActive = isActive;
		}

		public EventTypes getEventName()
		{
			return ((InternalData)@params).eventName;
		}

		public bool? getActive()
		{
			return ((InternalData)@params).isActive;
		}

		public OnEventChanged() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnEventChanged.ToString();
			@params = new InternalData();
		}
	}
}