﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnSystemRequest : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;

		public class InternalData
		{
			public RequestType requestType;
			public String url;
			public FileType fileType;
			public int? offset;
			public int? length;
			public int? timeout;
			public String fileName;
			public int? appID;
            public String requestSubType;
		}

		public void setRequestType(RequestType requestType)
		{
			((InternalData)@params).requestType = requestType;
		}

		public void setUrl(String url)
		{
			((InternalData)@params).url = url;
		}

        public void setRequestSubType(String requestSubType)
        {
            ((InternalData)@params).requestSubType = requestSubType;
        }

		public void setFileType(FileType fileType)
		{
			((InternalData)@params).fileType = fileType;
		}

		public void setOffset(int? offset)
		{
			((InternalData)@params).offset = offset;
		}

		public void setLength(int? length)
		{
			((InternalData)@params).length = length;
		}

		public void setTimeout(int? timeout)
		{
			((InternalData)@params).timeout = timeout;
		}

		public void setFileName(String fileName)
		{
			((InternalData)@params).fileName = fileName;
		}

		public void setAppId(int? id)
		{
			((InternalData)@params).appID = id;
		}

		public RequestType getRequestType()
		{
			return ((InternalData)@params).requestType;
		}

		public String getUrl()
		{
			return ((InternalData)@params).url;
		}

        public String getRequestSubType()
        {
            return ((InternalData)@params).requestSubType;
        }

		public FileType getFileType()
		{
			return ((InternalData)@params).fileType;
		}

		public int? getOffset()
		{
			return ((InternalData)@params).offset;
		}

		public int? getLength()
		{
			return ((InternalData)@params).length;
		}

		public int? getTimeout()
		{
			return ((InternalData)@params).timeout;
		}

		public String getFileName()
		{
			return ((InternalData)@params).fileName;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;		}

		public OnSystemRequest() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnSystemRequest.ToString();
			@params = new InternalData();
		}
	}
}