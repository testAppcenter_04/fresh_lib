﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using System;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutgoingResponses
{
	public class AllowDeviceToConnect : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public class InternalData : Base.Result
		{
			public bool? allow;
		}

		public void setAllow(bool? allow)
		{
			((InternalData)result).allow = allow;
		}

		public bool? getAllow()
		{
			return ((InternalData)result).allow;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.AllowDeviceToConnect.ToString();
		}

		public AllowDeviceToConnect() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}