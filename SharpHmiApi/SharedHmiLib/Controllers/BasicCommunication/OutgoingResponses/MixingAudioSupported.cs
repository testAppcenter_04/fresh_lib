﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.OutgoingResponses
{
	public class MixingAudioSupported : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public class InternalData : Base.Result
		{
			public bool? attenuatedSupported;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.MixingAudioSupported.ToString();
		}

		public void setAttenuatedSupported(bool? attenuatedSupported)
		{
			((InternalData)result).attenuatedSupported = attenuatedSupported;
		}

		public bool? getAttenuatedSupported()
		{
			return ((InternalData)result).attenuatedSupported;
		}

		public MixingAudioSupported() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
