﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using System;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutgoingResponses
{
	public class GetSystemInfo : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public class InternalData : Base.Result
		{
			public String ccpu_version;
			public Language language;
			public String wersCountryCode;
		}

		public void setCcpuVersion(String ccpu_version)
		{
			((InternalData)result).ccpu_version = ccpu_version;
		}

		public void setLanguage(Language language)
		{
			((InternalData)result).language = language;
		}

		public void setWersCountryCode(String wersCountryCode)
		{
			((InternalData)result).wersCountryCode = wersCountryCode;
		}

		public String getCcpuVersion()
		{
			return ((InternalData)result).ccpu_version;
		}

		public Language getLanguage()
		{
			return ((InternalData)result).language;
		}

		public String getWersCountryCode()
		{
			return ((InternalData)result).wersCountryCode;		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetSystemInfo.ToString();
		}

		public GetSystemInfo() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}