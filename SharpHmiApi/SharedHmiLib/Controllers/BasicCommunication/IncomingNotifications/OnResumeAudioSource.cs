﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingNotifications
{
	public class OnResumeAudioSource : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public InternalData data = null;
		public Object @params;

		public OnResumeAudioSource() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnResumeAudioSource.ToString();
		}

		public class InternalData
		{
			public int? appID;
		}

		public int? getAppID()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appID;
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

	}
}