﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingNotifications
{
	public class OnFileRemoved : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;
		public InternalData data = null;

		public class InternalData
		{
			public String fileName;
			public FileType fileType;
			public int? appID;
		}

		public String getFileName()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.fileName;
		}

		public FileType getFileType()
		{
			if (data == null)
				setData();

			return data.fileType;
		}

		public int? getAppID()
		{

			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appID;
		}

		public OnFileRemoved() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnFileRemoved.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}