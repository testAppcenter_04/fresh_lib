﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingNotifications
{
	public class OnAppRegistered : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public InternalData data = null;
		public Object @params;

		public OnAppRegistered() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnAppRegistered.ToString();
		}

		public class InternalData
		{
			public HMIApplication application;
			public List<TTSChunk> ttsName;
			public List<String> vrSynonyms;
			public bool? resumeVrGrammars;
			public AppPriority priority;
		}

		public HMIApplication getApplication()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.application;
		}

		public List<TTSChunk> getTtsNameList()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.ttsName;
		}

		public List<String> getVrSynonymsList()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.vrSynonyms;
		}

		public bool? getResumeVrGrammars()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.resumeVrGrammars;
		}

		public AppPriority getPriority()
		{
			if (data == null)
				setData();

			return data.priority;
		}


		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

	}
}
