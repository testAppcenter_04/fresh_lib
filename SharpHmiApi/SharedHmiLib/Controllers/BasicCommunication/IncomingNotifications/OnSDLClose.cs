﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingNotifications
{
	public class OnSDLClose : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public OnSDLClose() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnSDLClose.ToString();
		}
	}
}