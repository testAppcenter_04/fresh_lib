﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using System;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingNotifications
{
	public class OnPutFile : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public InternalData data = null;
		public Object @params;

		public OnPutFile() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnPutFile.ToString();
		}

		public class InternalData
		{
			public int? offset;
			public int? length;
			public int? fileSize;
			public String fileName;
			public String syncFileName;
			public FileType fileType;
			public bool? persistentFile;
		}

		public int? getOffset()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.offset;
		}

		public int? getLength()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.length;
		}

		public int? getFileSize()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.fileSize;
		}

		public String getFileName()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.fileName;
		}

		public String getSyncFileName()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.syncFileName;
		}

		public FileType getFileType()
		{
			if (data == null)
				setData();

			return data.fileType;
		}

		public bool? getPersistentFile()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.persistentFile;
		}


		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

	}
}