﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingNotifications
{
	public class OnSDLPersistenceComplete : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public OnSDLPersistenceComplete() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnSDLPersistenceComplete.ToString();
		}
	}
}