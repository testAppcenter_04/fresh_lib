﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
	public class DialNumber : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public class InternalData
		{
			public String number;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public String getNumber()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.number;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public DialNumber() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.DialNumber.ToString();
		}
	}
}