﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
	public class GetSystemInfo : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public GetSystemInfo() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetSystemInfo.ToString();
		}
	}
}