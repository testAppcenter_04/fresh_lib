﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.TTS.OutGoingNotifications
{
	public class OnLanguageChange : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.TTS;
		public Object @params;

		public class InternalData
		{
			public Language? language;
		}

		public void setLanguage(Language? language)
		{
			((InternalData)@params).language = language;
		}

		public Language? getLanguage()
		{
			return ((InternalData)@params).language;		}

		public OnLanguageChange() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnLanguageChange.ToString();
			@params = new InternalData();
		}
	}
}