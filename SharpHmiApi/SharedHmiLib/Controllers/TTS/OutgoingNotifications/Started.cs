﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.TTS.OutGoingNotifications
{
	public class Started : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.TTS;

		public Started() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.Started.ToString();
		}
	}
}
