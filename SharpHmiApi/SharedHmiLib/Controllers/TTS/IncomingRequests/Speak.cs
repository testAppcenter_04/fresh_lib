﻿using System;
using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.TTS.IncomingRequests
{
	public class Speak : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.TTS;
#pragma warning restore 0414

		public class InternalData
		{
			public List<TTSChunk> ttsChunks;
			public int? appID;
			public MethodName speakType;
			public bool? playTone;
		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public List<TTSChunk> getTtsChunkList()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.ttsChunks;
		}

		public MethodName getMethodName()
		{
			if (@params == null)
				setData();

			return @params.speakType;
		}

		public bool? getPlayTone()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.playTone;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public Speak() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.Speak.ToString();
		}
	}
}