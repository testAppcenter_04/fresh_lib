﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.TTS.IncomingRequests
{
	public class ChangeRegistration : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.TTS;
#pragma warning restore 0414

		public class InternalData
		{
			public List<TTSChunk> ttsName;
			public Language language;
			public int? appID;
		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public List<TTSChunk> getTtsName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.ttsName;
		}

		public Language getLanguage()
		{
			if (@params == null)
				setData();

			return @params.language;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public ChangeRegistration() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ChangeRegistration.ToString();
		}
	}
}