﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.TTS.IncomingRequests
{
	public class GetLanguage : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.TTS;
		public GetLanguage() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetLanguage.ToString();
		}
	}
}
