﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.SDL.IncomingNotifications
{
	public class OnAppPermissionChanged : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.SDL;
		public InternalData data = null;
		public Object @params;

		public class InternalData
		{
			public int? appID;
			public bool? isAppPermissionsRevoked;
			public List<PermissionItem> appRevokedPermissions;
			public bool? appRevoked;
			public bool? appPermissionsConsentNeeded;
			public bool? appUnauthorized;
			public AppPriority priority;
			public List<RequestType> requestType;
            public List<String> requestSubType;
		}

		public int? getAppID()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appID;
		}

		public bool? isAppPermissionsRevoked()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.isAppPermissionsRevoked;
		}

		public List<PermissionItem> getAppRevokedPermissions()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appRevokedPermissions;
		}

		public bool? isAppRevoked()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appRevoked;
		}

		public bool? isAppPermissionsConsentNeeded()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appPermissionsConsentNeeded;
		}

		public bool? isAppUnauthorized()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appUnauthorized;
		}

		public AppPriority getAppPriority()
		{
			if (data == null)
				setData();
			return data.priority;
		}

		public List<RequestType> getRequestType()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.requestType;
		}

        public List<String> getRequestSubType()
        {
            if (data == null)
                setData();
            if (data == null)
                return null;

            return data.requestSubType;
        }

		public OnAppPermissionChanged() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnAppPermissionChanged.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}