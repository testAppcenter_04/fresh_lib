﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using System.Collections.Generic;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.OutgoingRequests
{

	public class GetUserFriendlyMessage : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.SDL;

		public class InternalData
		{
			public List<String> messageCodes;
			public Language? language;
		}

		public GetUserFriendlyMessage() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetUserFriendlyMessage.ToString();
			@params = new InternalData();
		}

		public void setMessageCodes(List<String> messageCodes)
		{
			((InternalData)@params).messageCodes = messageCodes;
		}

		public void setLanguage(Language? language)
		{
			((InternalData)@params).language = language;
		}

		public List<String> getMessageCodes()
		{
			return ((InternalData)@params).messageCodes;
		}

		public Language? getLanguage()
		{
			return ((InternalData)@params).language;		}

	}
}