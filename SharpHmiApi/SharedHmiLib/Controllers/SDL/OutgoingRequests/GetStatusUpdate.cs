﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.OutgoingRequests
{

	public class GetStatusUpdate : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.SDL;

		public GetStatusUpdate() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetStatusUpdate.ToString();
		}

	}
}