﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.OutgoingRequests
{

	public class GetListOfPermissions : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.SDL;

		public class InternalData
		{
			public int? appID;
		}

		public GetListOfPermissions() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetListOfPermissions.ToString();
			@params = new InternalData();
		}

		public void setAppId(int? id)
		{
			((InternalData)@params).appID = id;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;
		}
	}
}