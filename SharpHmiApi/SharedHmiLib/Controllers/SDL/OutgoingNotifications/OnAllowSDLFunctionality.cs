﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.OutGoingNotifications
{
	public class OnAllowSDLFunctionality : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.SDL;
		public Object @params;

		public class InternalData
		{
			public DeviceInfo device;
			public bool? allowed;
			public ConsentSource? source;
		}

		public void setDevice(DeviceInfo device)
		{
			((InternalData)@params).device = device;
		}

		public void setAllowed(bool? allowed)
		{
			((InternalData)@params).allowed = allowed;
		}

		public void setSource(ConsentSource? source)
		{
			((InternalData)@params).source = source;
		}

		public DeviceInfo getDevice()
		{
			return ((InternalData)@params).device;
		}

		public bool? getAllowed()
		{
			return ((InternalData)@params).allowed;
		}

		public ConsentSource? getSource()
		{
			return ((InternalData)@params).source;		}

		public OnAllowSDLFunctionality() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnAllowSDLFunctionality.ToString();
			@params = new InternalData();
		}
	}
}