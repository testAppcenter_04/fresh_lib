﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.OutGoingNotifications
{
	public class OnPolicyUpdate : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.SDL;

		public OnPolicyUpdate() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnPolicyUpdate.ToString();
		}
	}
}