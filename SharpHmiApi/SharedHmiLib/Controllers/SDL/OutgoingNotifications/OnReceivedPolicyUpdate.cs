﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.OutGoingNotifications
{
	public class OnReceivedPolicyUpdate : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.SDL;
		public Object @params;

		public class InternalData
		{
			public String policyfile;
		}

		public void setPolicyfile(String policyfile)
		{
			((InternalData)@params).policyfile = policyfile;
		}

		public String getPolicyfile()
		{
			return ((InternalData)@params).policyfile;
		}

		public OnReceivedPolicyUpdate() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnReceivedPolicyUpdate.ToString();
			@params = new InternalData();
		}
	}
}