﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Manager;
using HmiApiLib.Controllers.SDL.OutgoingRequests;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using HmiApiLib.Handshaking;
using System.Collections.Generic;
using HmiApiLib.Common.Enums;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.RC.OutGoingNotifications;

namespace HmiApiLib.Builder
{
	public class BuildRpc
	{
		static int id = 0;

		public static int getNextId()
		{
			id = id + 100;
			return id;
		}

		public static string buildRegisterComponent(InterfaceType type)
		{
			RegisterComponent regComponent = new RegisterComponent();
			regComponent.setComponent(type);

			ConnectionManager.Instance.handleInputConsoleLog(new RpcLogMessage(regComponent));

			string json = Newtonsoft.Json.JsonConvert.SerializeObject(regComponent);

			return json;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnReady()
		{
			OnReady ready = new OnReady();
            return ready;
		}

        public static RequestNotifyMessage buildBasicCommunicationOnAppActivated(int? appId,int? windowId)
		{
			OnAppActivated appActivated = new OnAppActivated();
			appActivated.setAppId(appId);
			appActivated.setWindowId(windowId);

			return appActivated;
		}

        public static RequestNotifyMessage buildBasicCommunicationOnAppDeactivated(int? appId, int? windowId)
		{
			OnAppDeactivated appDeActivated = new OnAppDeactivated();
			appDeActivated.setAppId(appId);
			appDeActivated.setWindowId(windowId);

			return appDeActivated;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnExitApplication(ApplicationExitReason? applicationExitReason, int? appId)
		{
			OnExitApplication exitApplication = new OnExitApplication();

            if (applicationExitReason != null)
                exitApplication.setApplicationExitReason((ApplicationExitReason)applicationExitReason);
            
            if (appId != null)
                exitApplication.setAppId(appId);

			return exitApplication;
		}

		public static RequestNotifyMessage buildRcOnInteriorVehicleData(ModuleData moduleData)
		{
			OnInteriorVehicleData onInteriorVehicleData = new OnInteriorVehicleData();

			if (moduleData != null)
                onInteriorVehicleData.setModuleData(moduleData);

			return onInteriorVehicleData;
		}

		public static RequestNotifyMessage buildRcOnRemoteControlSettings(bool? allowed, RCAccessMode? accessMode)
		{
			OnRemoteControlSettings onRemoteControlSettings = new OnRemoteControlSettings();

			if (allowed != null)
                onRemoteControlSettings.setAllowed(allowed);

			if (accessMode != null)
                onRemoteControlSettings.setAccessMode(accessMode);

			return onRemoteControlSettings;
		}

		public static RpcResponse buildRcGetCapabilitiesResponse(int corrId, Common.Enums.Result? resultCode, RemoteControlCapabilities remoteControlCapabilities)
		{
			Controllers.RC.OutgoingResponses.GetCapabilities msg = new Controllers.RC.OutgoingResponses.GetCapabilities();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

            if (remoteControlCapabilities != null)
                msg.setRemoteControlCapabilities(remoteControlCapabilities);

			return msg;
		}

		public static RpcResponse buildRcGetInteriorVehicleDataResponse(int corrId, Common.Enums.Result? resultCode, ModuleData moduleData, bool? isSubscribed)
		{
			Controllers.RC.OutgoingResponses.GetInteriorVehicleData msg = new Controllers.RC.OutgoingResponses.GetInteriorVehicleData();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			if (moduleData != null)
                msg.setModuleData(moduleData);

			if (isSubscribed != null)
                msg.setIsSubscribed(isSubscribed);

			return msg;
		}

		public static RpcResponse buildRcGetInteriorVehicleDataConsentResponse(int corrId, Common.Enums.Result? resultCode, bool? allowed, bool? success, string info)
		{
			Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent msg = new Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			if (allowed != null)
				msg.setAllowed(allowed);

			if (success != null)
				msg.setSuccess(success);

			if (info != null)
				msg.setInfo(info);

			return msg;
		}

		public static RpcResponse buildUIDeleteWindowResponse(int corrId, Common.Enums.Result? resultCode, bool? success,String info)
		{
			Controllers.UI.OutgoingResponses.DeleteWindow msg = new Controllers.UI.OutgoingResponses.DeleteWindow();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			if (success != null)
				msg.setSuccess(success);
			if (info != null)
				msg.setInfo(info);

			return msg;
		}

		public static RpcResponse buildUICreateWindowResponse(int corrId, Common.Enums.Result? resultCode, bool? success, String info)
		{
			Controllers.UI.OutgoingResponses.CreateWindow msg = new Controllers.UI.OutgoingResponses.CreateWindow();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			if (success != null)
				msg.setSuccess(success);
			if (info != null)
				msg.setInfo(info);

			return msg;
		}
		public static RpcResponse buildRcReleaseInteriorVehicleDataConsentResponse(int corrId, Common.Enums.Result? resultCode, bool? allowed,string info)
		{
			Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent msg = new Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			if (allowed != null)
				msg.setSuccess(allowed);

			if (info != null)
				msg.setInfo(info);

			return msg;
		}
		public static RpcResponse buildRcIsReadyResponse(int corrId, Common.Enums.Result? resultCode, bool? available)
		{
			Controllers.RC.OutgoingResponses.IsReady msg = new Controllers.RC.OutgoingResponses.IsReady();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			if (available != null)
                msg.setAvailable(available);

			return msg;
		}

		public static RpcResponse buildRcSetInteriorVehicleDataResponse(int corrId, Common.Enums.Result? resultCode, ModuleData moduleData)
		{
			Controllers.RC.OutgoingResponses.SetInteriorVehicleData msg = new Controllers.RC.OutgoingResponses.SetInteriorVehicleData();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			if (moduleData != null)
                msg.setModuleData(moduleData);

			return msg;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnIgnitionCycleOver()
		{
			OnIgnitionCycleOver ignitionCycleOver = new OnIgnitionCycleOver();

			return ignitionCycleOver;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnStartDeviceDiscovery()
		{
			OnStartDeviceDiscovery startDeviceDiscovery = new OnStartDeviceDiscovery();

			return startDeviceDiscovery;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnUpdateDeviceList()
		{
			OnUpdateDeviceList updateDeviceList = new OnUpdateDeviceList();

			return updateDeviceList;
		}

        public static RequestNotifyMessage buildBasicCommunicationOnSystemTimeReady()
        {
            OnSystemTimeReady onSystemTimeReady = new OnSystemTimeReady();

            return onSystemTimeReady;
        }

		public static RequestNotifyMessage buildBasicCommunicationOnSystemInfoChanged(Language? language)
		{
			OnSystemInfoChanged systemInfoChanged = new OnSystemInfoChanged();

            if (language != null)
                systemInfoChanged.setLanguage(language);

			return systemInfoChanged;
		}

		public static RequestNotifyMessage buildButtonsOnButtonEvent(ButtonName? name, ButtonEventMode? mode, int? customButtonID, int? appID)
		{
            Controllers.Buttons.OutGoingNotifications.OnButtonEvent buttonEvent = new Controllers.Buttons.OutGoingNotifications.OnButtonEvent();

			if (name != null)
                buttonEvent.setName(name);

			if (mode != null)
                buttonEvent.setMode(mode);

			if (customButtonID != null)
                buttonEvent.setCustomButtonID(customButtonID);

			if (appID != null)
                buttonEvent.setAppId(appID);

			return buttonEvent;
		}

		public static RequestNotifyMessage buildButtonsOnButtonPress(ButtonName? name, ButtonPressMode? mode, int? customButtonID, int? appID)
		{
			Controllers.Buttons.OutGoingNotifications.OnButtonPress buttonPress = new Controllers.Buttons.OutGoingNotifications.OnButtonPress();

			if (name != null)
				buttonPress.setName(name);

			if (mode != null)
				buttonPress.setMode(mode);

			if (customButtonID != null)
				buttonPress.setCustomButtonID(customButtonID);

			if (appID != null)
				buttonPress.setAppId(appID);

			return buttonPress;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnExitAllApplications(ApplicationsCloseReason? applicationsCloseReason)
		{
			OnExitAllApplications exitAllApplications = new OnExitAllApplications();

            if (applicationsCloseReason != null)
                exitAllApplications.setApplicationsCloseReason((ApplicationsCloseReason)applicationsCloseReason);

			return exitAllApplications;
		}

		public static RequestNotifyMessage buildUiOnSystemContext(SystemContext? context, int? appId ,int? windowId)
		{
			Controllers.UI.OutGoingNotifications.OnSystemContext systemContext = new Controllers.UI.OutGoingNotifications.OnSystemContext();

            if (context != null)
                systemContext.setSystemContext((SystemContext)context);

			systemContext.setAppId(appId);
			systemContext.setWindowId(windowId);

			return systemContext;
		}

		public static RequestNotifyMessage buildUiOnTouchEvent(TouchType? type, List<TouchEvent> _event)
		{
			Controllers.UI.OutGoingNotifications.OnTouchEvent onTouchEvent = new Controllers.UI.OutGoingNotifications.OnTouchEvent();

			if (type != null)
                onTouchEvent.setTouchType(type);

			if (_event != null)
                onTouchEvent.setTouchEvent(_event);

			return onTouchEvent;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnAwakeSDL()
		{
            OnAwakeSDL awakeSDL = new OnAwakeSDL();

			return awakeSDL;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnDeactivateHMINotification(bool? isDeactivated)
		{
            OnDeactivateHMI deactivateHmi = new OnDeactivateHMI();

            if(isDeactivated != null)
            deactivateHmi.deactivate(isDeactivated);

			return deactivateHmi;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnEmergencyEventNotification(bool? enabled)
		{
            OnEmergencyEvent emergencyEvent = new OnEmergencyEvent();

			if (enabled != null)
                emergencyEvent.setEnabled(enabled);

			return emergencyEvent;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnPhoneCallNotification(bool? isActive)
		{
            OnPhoneCall phoneCall = new OnPhoneCall();

			if (isActive != null)
                phoneCall.setActive(isActive);

			return phoneCall;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnDeviceChosenNotification(DeviceInfo deviceInfo)
		{
            OnDeviceChosen deviceChosen = new OnDeviceChosen();

			if (deviceInfo != null)
                deviceChosen.setDeviceInfo(deviceInfo);

			return deviceChosen;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnEventChangedNotification(EventTypes? eventName, bool? isActive)
		{
			OnEventChanged onEventChanged = new OnEventChanged();

            if (eventName != null)
                onEventChanged.setEventName((EventTypes)eventName);

            if (isActive != null)
                onEventChanged.setActive(isActive);

			return onEventChanged;
		}

		public static RequestNotifyMessage buildBasicCommunicationOnFindApplicationsNotification(DeviceInfo deviceInfo)
		{
			OnFindApplications findApplications = new OnFindApplications();

			if (findApplications != null)
                findApplications.setDeviceInfo(deviceInfo);

			return findApplications;
		}

        public static RequestNotifyMessage buildBasicCommunicationOnSystemRequestNotification(RequestType? requestType, String url, FileType? fileType, int? offset, int? length, int? timeout, String fileName, int? appID, String requestSubType)
		{
			OnSystemRequest systemRequest = new OnSystemRequest();

            if (requestType != null)
                systemRequest.setRequestType((RequestType)requestType);

            if (url != null)
                systemRequest.setUrl(url);

            if (fileType != null)
                systemRequest.setFileType((FileType)fileType);

            if (offset != null)
                systemRequest.setOffset(offset);

            if (length != null)
                systemRequest.setLength(length);

            if (timeout != null)
                systemRequest.setTimeout(timeout);

            if (fileName != null)
                systemRequest.setFileName(fileName);

            if (appID != null)
                systemRequest.setAppId(appID);

            if (requestSubType != null)
                systemRequest.setRequestSubType(requestSubType);

			return systemRequest;
		}

		public static string buildSubscribeToNotification(ComponentPrefix? prefix, FunctionType? type)
		{
			OnButtonSubscription notification = new OnButtonSubscription();

            if ((prefix != null) && (type != null)) {
				notification.setPropertyName((ComponentPrefix)prefix, (FunctionType)type);                
            }

			ConnectionManager.Instance.handleInputConsoleLog(new RpcLogMessage(notification));
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(notification);

			return json;
		}

        public static string buildSubscribeToNotification(PropertyName propertyName)
		{
			OnButtonSubscription notification = new OnButtonSubscription();

            if (propertyName != null)
			{
                notification.setPropertyName(propertyName);
			}

			ConnectionManager.Instance.handleInputConsoleLog(new RpcLogMessage(notification));
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(notification);

			return json;
		}

		public static RpcRequest buildSdlActivateAppRequest(int corrId, int? appId)
		{
			ActivateApp msg = new ActivateApp();
			msg.setId(corrId);

			if (appId != null)
				msg.setAppId(appId);

			return msg;
		}

		public static RpcRequest buildSDLUpdateSDLRequest(int corrId)
		{
            UpdateSDL msg = new UpdateSDL();
			msg.setId(corrId);

			return msg;
		}

		public static RpcRequest buildSDLGetUserFriendlyMessageRequest(int corrId, List<String> messageCodes, Language? language)
		{
			GetUserFriendlyMessage msg = new GetUserFriendlyMessage();
			msg.setId(corrId);

            if (messageCodes != null)
                msg.setMessageCodes(messageCodes);

            if (language != null)
                msg.setLanguage(language);
            
			return msg;
		}

		public static RpcRequest buildSDLGetURLSRequest(int corrId, int? service)
		{
			GetURLS msg = new GetURLS();
			msg.setId(corrId);

			if (service != null)
                msg.setService(service);

			return msg;
		}

		public static RpcRequest buildSDLGetStatusUpdateRequest(int corrId)
		{
			GetStatusUpdate msg = new GetStatusUpdate();
			msg.setId(corrId);

			return msg;
		}

		public static RpcRequest buildSDLGetListOfPermissionsRequest(int corrId, int? appID)
		{
			GetListOfPermissions msg = new GetListOfPermissions();
			msg.setId(corrId);

            if (appID != null)
                msg.setAppId(appID);

			return msg;
		}

		public static RequestNotifyMessage buildNavigationOnTBTClientState(TBTState? state)
		{
			Controllers.Navigation.OutGoingNotifications.OnTBTClientState tbtClientState = new Controllers.Navigation.OutGoingNotifications.OnTBTClientState();

			if (state != null)
                tbtClientState.setState(state);

			return tbtClientState;
		}

		public static RequestNotifyMessage buildSDLOnReceivedPolicyUpdate(String policyfile)
		{
			Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate receivedPolicyUpdate = new Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate();

			if (receivedPolicyUpdate != null)
                receivedPolicyUpdate.setPolicyfile(policyfile);

			return receivedPolicyUpdate;
		}

		public static RequestNotifyMessage buildSDLOnPolicyUpdate()
		{
			Controllers.SDL.OutGoingNotifications.OnPolicyUpdate policyUpdate = new Controllers.SDL.OutGoingNotifications.OnPolicyUpdate();

			return policyUpdate;
		}

		public static RequestNotifyMessage buildSDLOnAppPermissionConsent(int? appID, List<PermissionItem> consentedFunctions, ConsentSource? source)
		{
			Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent appPermissionConsent = new Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent();

			if (appID != null)
                appPermissionConsent.setAppId(appID);

			if (consentedFunctions != null)
                appPermissionConsent.setConsentedFunctions(consentedFunctions);

			if (source != null)
                appPermissionConsent.setSource(source);

			return appPermissionConsent;
		}

		public static RequestNotifyMessage buildSDLOnAllowSDLFunctionality(DeviceInfo device, bool? allowed, ConsentSource? source)
		{
			Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality allowSDLFunctionality = new Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality();

			if (device != null)
                allowSDLFunctionality.setDevice(device);

			if (source != null)
                allowSDLFunctionality.setSource(source);

			if (allowed != null)
                allowSDLFunctionality.setAllowed(allowed);

			return allowSDLFunctionality;
		}

		public static RpcResponse buildUiSetAppIconResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.SetAppIcon msg = new Controllers.UI.OutgoingResponses.SetAppIcon();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);
            
			return msg;
		}

		public static RpcResponse buildUiShowResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.Show msg = new Controllers.UI.OutgoingResponses.Show();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);
            
			return msg;
		}

		public static RpcResponse buildUiAlertResponse(int corrId, Common.Enums.Result? resultCode, int? tryAgainTime)
		{
			Controllers.UI.OutgoingResponses.Alert msg = new Controllers.UI.OutgoingResponses.Alert();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (tryAgainTime != null)
				msg.setTryAgainTime(tryAgainTime);

			return msg;
		}

		public static RpcResponse buildUiPerformInteractionResponse(int corrId, int? choiceId, String manualTextEntry, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.PerformInteraction msg = new Controllers.UI.OutgoingResponses.PerformInteraction();
			msg.setId(corrId);

			if (choiceId != null)
			{
				msg.setChoiceID(choiceId);
			}

			if (manualTextEntry != null)
			{
				msg.setManualTextEntry(manualTextEntry);
			}

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiAddCommandResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.AddCommand msg = new Controllers.UI.OutgoingResponses.AddCommand();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiAddSubMenuResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.AddSubMenu msg = new Controllers.UI.OutgoingResponses.AddSubMenu();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);
            
			return msg;
		}

        public static RpcResponse buildUiShowAppMenuResponse(int corrId, Common.Enums.Result? resultCode)
        {
            Controllers.UI.OutgoingResponses.ShowAppMenu msg = new Controllers.UI.OutgoingResponses.ShowAppMenu();
            msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            return msg;
        }

        public static RpcResponse buildUiCloseApplicationResponse(int corrId, Common.Enums.Result? resultCode)
        {
            Controllers.UI.OutgoingResponses.CloseApplication msg = new Controllers.UI.OutgoingResponses.CloseApplication();
            msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            return msg;
        }

        public static RpcResponse buildUiChangeRegistrationResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.ChangeRegistration msg = new Controllers.UI.OutgoingResponses.ChangeRegistration();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiClosePopUpResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.ClosePopUp msg = new Controllers.UI.OutgoingResponses.ClosePopUp();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiDeleteCommandResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.DeleteCommand msg = new Controllers.UI.OutgoingResponses.DeleteCommand();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiDeleteSubMenuResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.DeleteSubMenu msg = new Controllers.UI.OutgoingResponses.DeleteSubMenu();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiEndAudioPassThruResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.EndAudioPassThru msg = new Controllers.UI.OutgoingResponses.EndAudioPassThru();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

        public static RpcResponse buildUiGetCapabilitiesResponse(int corrId, Common.Enums.Result? resultCode, DisplayCapabilities displayCapabilities, AudioPassThruCapabilities audioPassThruCapabilities, HmiZoneCapabilities? hmiZoneCapabilities, List<SoftButtonCapabilities> softButtonCapabilities, HMICapabilities hmiCapabilities, SystemCapabilities systemCapabilities)
		{
			Controllers.UI.OutgoingResponses.GetCapabilities msg = new Controllers.UI.OutgoingResponses.GetCapabilities();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (audioPassThruCapabilities != null)
				msg.setAudioPassThruCapabilities(audioPassThruCapabilities);

			if (hmiCapabilities != null)
				msg.setHMICapabilities(hmiCapabilities);

			if (displayCapabilities != null)
				msg.setDisplayCapabilities(displayCapabilities);

            if (hmiZoneCapabilities != null)
                msg.setHmiZoneCapabilities((HmiZoneCapabilities)hmiZoneCapabilities);

			if (softButtonCapabilities != null)
				msg.setSoftButtonCapabilities(softButtonCapabilities);

            if (systemCapabilities != null)
                msg.setSystemCapabilities(systemCapabilities);

			return msg;
		}

		public static RpcResponse buildUiGetSupportedLanguagesResponse(int corrId, Common.Enums.Result? resultCode, List<Language> languages)
		{
			Controllers.UI.OutgoingResponses.GetSupportedLanguages msg = new Controllers.UI.OutgoingResponses.GetSupportedLanguages();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (languages != null)
				msg.setLanguage(languages);

			return msg;
		}

		public static RpcResponse buildUiPerformAudioPassThruResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.PerformAudioPassThru msg = new Controllers.UI.OutgoingResponses.PerformAudioPassThru();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiScrollableMessageResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.ScrollableMessage msg = new Controllers.UI.OutgoingResponses.ScrollableMessage();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiSetDisplayLayoutResponse(int corrId, Common.Enums.Result? resultCode, DisplayCapabilities displayCapabilities, List<ButtonCapabilities> buttonCapabilities, List<SoftButtonCapabilities> softButtonCapabilities, PresetBankCapabilities presetBankCapabilities)
		{
			Controllers.UI.OutgoingResponses.SetDisplayLayout msg = new Controllers.UI.OutgoingResponses.SetDisplayLayout();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (displayCapabilities != null)
				msg.setDisplayCapabilities(displayCapabilities);

			if (buttonCapabilities != null)
				msg.setButtonCapabilities(buttonCapabilities);

			if (presetBankCapabilities != null)
				msg.setPresetBankCapabilities(presetBankCapabilities);

			if (softButtonCapabilities != null)
				msg.setSoftButtonCapabilities(softButtonCapabilities);

			return msg;
		}

		public static RpcResponse buildUiSetGlobalPropertiesResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.SetGlobalProperties msg = new Controllers.UI.OutgoingResponses.SetGlobalProperties();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiSetMediaClockTimerResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.SetMediaClockTimer msg = new Controllers.UI.OutgoingResponses.SetMediaClockTimer();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiShowCustomFormResponse(int corrId, Common.Enums.Result? resultCode, String info)
		{
			Controllers.UI.OutgoingResponses.ShowCustomForm msg = new Controllers.UI.OutgoingResponses.ShowCustomForm();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (info != null)
				msg.setInfo(info);

			return msg;
		}

		public static RpcResponse buildUiSliderResponse(int corrId, Common.Enums.Result? resultCode, int? sliderPosition)
		{
			Controllers.UI.OutgoingResponses.Slider msg = new Controllers.UI.OutgoingResponses.Slider();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (sliderPosition != null)
				msg.setSliderPosition(sliderPosition);

			return msg;
		}

		public static RpcResponse buildVrAddCommandResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.VR.OutgoingResponses.AddCommand msg = new Controllers.VR.OutgoingResponses.AddCommand();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildVrChangeRegistrationResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.VR.OutgoingResponses.ChangeRegistration msg = new Controllers.VR.OutgoingResponses.ChangeRegistration();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildVrGetCapabilitiesResponse(int corrId, Common.Enums.Result? resultCode, List<VrCapabilities> vrCapabilities)
		{
			Controllers.VR.OutgoingResponses.GetCapabilities msg = new Controllers.VR.OutgoingResponses.GetCapabilities();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (vrCapabilities != null)
				msg.setVrCapabilities(vrCapabilities);
		
			return msg;
		}

		public static RpcResponse buildVrGetSupportedLanguagesResponse(int corrId, Common.Enums.Result? resultCode, List<Language> language)
		{
			Controllers.VR.OutgoingResponses.GetSupportedLanguages msg = new Controllers.VR.OutgoingResponses.GetSupportedLanguages();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (language != null)
				msg.setLanguages(language);

			return msg;
		}

		public static RpcResponse buildVrPerformInteractionResponse(int corrId, int? choiceId, Common.Enums.Result? resultCode)
		{
			Controllers.VR.OutgoingResponses.PerformInteraction msg = new Controllers.VR.OutgoingResponses.PerformInteraction();
			msg.setId(corrId);

			if (choiceId != null)
			{
				msg.setChoiceID(choiceId);
			}

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}


		public static RpcResponse buildVrDeleteCommandResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.VR.OutgoingResponses.DeleteCommand msg = new Controllers.VR.OutgoingResponses.DeleteCommand();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildTtsSpeakResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.TTS.OutgoingResponses.Speak msg = new Controllers.TTS.OutgoingResponses.Speak();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RequestNotifyMessage buildTtsStoppedNotification()
		{
            Controllers.TTS.OutGoingNotifications.Stopped msg = new Controllers.TTS.OutGoingNotifications.Stopped();

			return msg;
		}

		public static RequestNotifyMessage buildTtsStartedNotification()
		{
			Controllers.TTS.OutGoingNotifications.Started msg = new Controllers.TTS.OutGoingNotifications.Started();

			return msg;
		}

		public static RequestNotifyMessage buildTtsOnResetTimeout(int? appID, String methodName)
		{
			Controllers.TTS.OutGoingNotifications.OnResetTimeout msg = new Controllers.TTS.OutGoingNotifications.OnResetTimeout();

            if (appID != null)
                msg.setAppId(appID);

			if (methodName != null)
                msg.setMethodName(methodName);

			return msg;
		}

		public static RequestNotifyMessage buildTtsOnLanguageChange(Language? language)
		{
			Controllers.TTS.OutGoingNotifications.OnLanguageChange msg = new Controllers.TTS.OutGoingNotifications.OnLanguageChange();

			if (language != null)
                msg.setLanguage(language);

			return msg;
		}

		public static RequestNotifyMessage buildUIOnResetTimeout(int? appID, String methodName)
		{
			Controllers.UI.OutGoingNotifications.OnResetTimeout msg = new Controllers.UI.OutGoingNotifications.OnResetTimeout();

			if (appID != null)
                msg.setAppId(appID);

			if (methodName != null)
                msg.setMethodName(methodName);

			return msg;
		}

		public static RequestNotifyMessage buildUIOnLanguageChange(Language? language)
		{
			Controllers.UI.OutGoingNotifications.OnLanguageChange msg = new Controllers.UI.OutGoingNotifications.OnLanguageChange();

			if (language != null)
                msg.setLanguage(language);

			return msg;
		}

		public static RequestNotifyMessage buildUIOnKeyboardInput(KeyboardEvent? _event, String data)
		{
			Controllers.UI.OutGoingNotifications.OnKeyboardInput msg = new Controllers.UI.OutGoingNotifications.OnKeyboardInput();

			if (_event != null)
                msg.setKeyboardEvent(_event);

			if (data != null)
                msg.setData(data);

			return msg;
		}

		public static RequestNotifyMessage buildUIOnDriverDistraction(DriverDistractionState? state)
		{
			Controllers.UI.OutGoingNotifications.OnDriverDistraction msg = new Controllers.UI.OutGoingNotifications.OnDriverDistraction();

			if (state != null)
                msg.setDriverDistractionState(state);

			return msg;
		}

        public static RequestNotifyMessage buildUIOnSeekMediaClockTimer(TimeFormat seekTime, int? appId)
        {
            Controllers.UI.OutGoingNotifications.OnSeekMediaClockTimer msg = new Controllers.UI.OutGoingNotifications.OnSeekMediaClockTimer();

            if (seekTime != null)
                msg.setSeekTime(seekTime);

            if (appId != null)
                msg.setAppId(appId);

            return msg;
        }

        public static RequestNotifyMessage buildUIOnCommand(int? cmdID, int? appID)
		{
			Controllers.UI.OutGoingNotifications.OnCommand msg = new Controllers.UI.OutGoingNotifications.OnCommand();

			if (cmdID != null)
                msg.setCmdID(cmdID);

			if (appID != null)
                msg.setAppId(appID);

			return msg;
		}

		public static RequestNotifyMessage buildVROnCommand(int? cmdID, int? appID)
		{
			Controllers.VR.OutGoingNotifications.OnCommand msg = new Controllers.VR.OutGoingNotifications.OnCommand();

			if (cmdID != null)
				msg.setCmdID(cmdID);

			if (appID != null)
				msg.setAppId(appID);

			return msg;
		}

		public static RequestNotifyMessage buildVROnLanguageChange(Language? language)
		{
			Controllers.VR.OutGoingNotifications.OnLanguageChange msg = new Controllers.VR.OutGoingNotifications.OnLanguageChange();

			if (language != null)
                msg.setLanguage(language);

			return msg;
		}

		public static RequestNotifyMessage buildVRStartedNotification()
		{
            Controllers.VR.OutGoingNotifications.Started msg = new Controllers.VR.OutGoingNotifications.Started();

			return msg;
		}

		public static RequestNotifyMessage buildVRStoppedNotification()
		{
            Controllers.VR.OutGoingNotifications.Stopped msg = new Controllers.VR.OutGoingNotifications.Stopped();

			return msg;
		}

		public static RpcResponse buildTtsStopSpeakingResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.TTS.OutgoingResponses.StopSpeaking msg = new Controllers.TTS.OutgoingResponses.StopSpeaking();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildButtonsGetCapabilitiesResponse(int corrId, List<ButtonCapabilities> capabilities, PresetBankCapabilities presetBankCapabilities, Common.Enums.Result? resultCode)
		{
			Controllers.Buttons.OutgoingResponses.GetCapabilities msg = new Controllers.Buttons.OutgoingResponses.GetCapabilities();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (capabilities != null)
				msg.setButtonCapabilities(capabilities);

			if (presetBankCapabilities != null)
				msg.setPresetBankCapabilities(presetBankCapabilities);

            return msg;
		}

		public static RpcResponse buildBasicCommunicationMixingAudioSupportedResponse(int corrId, bool? attenuatedSupported, Common.Enums.Result? resultCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported msg = new Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (attenuatedSupported != null)
				msg.setAttenuatedSupported(attenuatedSupported);

			return msg;
		}

		public static RpcResponse buildBasicCommunicationActivateAppResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.ActivateApp msg = new Controllers.BasicCommunication.OutgoingResponses.ActivateApp();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            return msg;
		}

        public static RpcResponse buildButtonsButtonPressResponse(int corrId, Common.Enums.Result? resultCode)
		{
            Controllers.Buttons.OutgoingResponses.ButtonPress msg = new Controllers.Buttons.OutgoingResponses.ButtonPress();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildUiGetLanguageResponse(int corrId, Language? language, Common.Enums.Result? resultCode)
		{
			Controllers.UI.OutgoingResponses.GetLanguage msg = new Controllers.UI.OutgoingResponses.GetLanguage();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            if (language != null)
                msg.setLanguage((Common.Enums.Language)language);

			return msg;
		}

		public static RpcResponse buildVrGetLanguageResponse(int corrId, Language? language, Common.Enums.Result? resultCode)
		{
			Controllers.VR.OutgoingResponses.GetLanguage msg = new Controllers.VR.OutgoingResponses.GetLanguage();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            if (language != null)
                msg.setLanguage((Language)language);

			return msg;
		}

		public static RpcResponse buildTtsGetLanguageResponse(int corrId, Language? language, Common.Enums.Result? resultCode)
		{
			Controllers.TTS.OutgoingResponses.GetLanguage msg = new Controllers.TTS.OutgoingResponses.GetLanguage();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            if (language != null)
                msg.setLanguage((Common.Enums.Language)language);

			return msg;
		}

		public static RpcResponse buildIsReadyResponse(int corrId, InterfaceType? type, bool? available, Common.Enums.Result? resultCode)
		{
			RpcResponse msg = null;

            if(type != null) {
				if (type == InterfaceType.VR)
				{
					msg = new Controllers.VR.OutgoingResponses.IsReady();
					((Controllers.VR.OutgoingResponses.IsReady)msg).setAvailable(available);
				}
				else if (type == InterfaceType.TTS)
				{
					msg = new Controllers.TTS.OutgoingResponses.IsReady();
					((Controllers.TTS.OutgoingResponses.IsReady)msg).setAvailable(available);
				}
				else if (type == InterfaceType.UI)
				{
					msg = new Controllers.UI.OutgoingResponses.IsReady();
					((Controllers.UI.OutgoingResponses.IsReady)msg).setAvailable(available);
				}
				else if (type == InterfaceType.Navigation)
				{
					msg = new Controllers.Navigation.OutgoingResponses.IsReady();
					((Controllers.Navigation.OutgoingResponses.IsReady)msg).setAvailable(available);
				}
				else if (type == InterfaceType.VehicleInfo)
				{
					msg = new Controllers.VehicleInfo.OutgoingResponses.IsReady();
					((Controllers.VehicleInfo.OutgoingResponses.IsReady)msg).setAvailable(available);
				}
                else if (type == InterfaceType.RC)
				{
                    msg = new Controllers.RC.OutgoingResponses.IsReady();
					((Controllers.RC.OutgoingResponses.IsReady)msg).setAvailable(available);
				}
			}

            if(msg != null){
				msg.setId(corrId);

                if (resultCode != null)
                    msg.setResultCode((Common.Enums.Result)resultCode);
			}

			return msg;
		}

		public static RpcResponse buildVehicleInfoDiagnosticMessageResponse(int corrId, Common.Enums.Result? resultCode, List<int> messageDataResult)
		{
			Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage msg = new Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (messageDataResult != null)
				msg.setMessageDataResult(messageDataResult);

			return msg;
		}

		public static RpcResponse buildVehicleInfoGetDTCsResponse(int corrId, Common.Enums.Result? resultCode, int? ecuHeader, List<String> dtc)
		{
			Controllers.VehicleInfo.OutgoingResponses.GetDTCs msg = new Controllers.VehicleInfo.OutgoingResponses.GetDTCs();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (ecuHeader != null)
				msg.setEcuHeader(ecuHeader);

			if (dtc != null)
				msg.setDtc(dtc);

			return msg;
		}

		public static RpcResponse buildVehicleInfoGetVehicleDataResponse(int corrId, Common.Enums.Result? resultCode, GPSData gps, float? speed, int? rpm, float? fuelLevel,
                                                                         ComponentVolumeStatus? fuelLevel_State, float? instantFuelConsumption, float? externalTemperature, string vin,
                                                                         PRNDL? prndl, TurnSignal? turnSignal, TireStatus tirePressure, int? odometer, BeltStatus beltStatus, BodyInformation bodyInformation,
                                                                        DeviceStatus deviceStatus, VehicleDataEventStatus? driverBraking, WiperStatus? wiperStatus, HeadLampStatus headLampStatus,
                                                                        float? engineTorque, float? accPedalPosition, float? steeringWheelAngle, ECallInfo eCallInfo, AirbagStatus airbagStatus,
                                                                         EmergencyEvent emergencyEvent, ClusterModeStatus clusterModes, MyKey myKey, ElectronicParkBrakeStatus? electronicParkBrakeStatus,
                                                                        float? engineOilLife, List<FuelRange> fuelRange)
		{
			Controllers.VehicleInfo.OutgoingResponses.GetVehicleData msg = new Controllers.VehicleInfo.OutgoingResponses.GetVehicleData();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (gps != null)
				msg.setGps(gps);

			if (speed != null)
				msg.setSpeed(speed);

			if (rpm != null)
				msg.setRpm(rpm);

			if (fuelLevel != null)
				msg.setFuelLevel(fuelLevel);

			if (fuelLevel_State != null)
				msg.setFuelLevel_State(fuelLevel_State);

			if (instantFuelConsumption != null)
				msg.setInstantFuelConsumption(instantFuelConsumption);

			if (externalTemperature != null)
				msg.setExternalTemperature(externalTemperature);

			if (vin != null)
				msg.setVin(vin);

			if (prndl != null)
				msg.setPrndl(prndl);

            if (turnSignal != null)
                msg.setTurnSignal(turnSignal);

			if (tirePressure != null)
				msg.setTirePressure(tirePressure);

			if (odometer != null)
				msg.setOdometer(odometer);

			if (beltStatus != null)
				msg.setBeltStatus(beltStatus);

			if (bodyInformation != null)
				msg.setBodyInformation(bodyInformation);

			if (deviceStatus != null)
				msg.setDeviceStatus(deviceStatus);

			if (driverBraking != null)
				msg.setDriverBraking(driverBraking);

			if (wiperStatus != null)
				msg.setWiperStatus(wiperStatus);

			if (headLampStatus != null)
				msg.setHeadLampStatus(headLampStatus);

			if (engineTorque != null)
				msg.setEngineTorque(engineTorque);

			if (accPedalPosition != null)
				msg.setAccPedalPosition(accPedalPosition);

			if (steeringWheelAngle != null)
				msg.setSteeringWheelAngle(steeringWheelAngle);

			if (eCallInfo != null)
				msg.setECallInfo(eCallInfo);

			if (airbagStatus != null)
				msg.setAirbagStatus(airbagStatus);

			if (emergencyEvent != null)
				msg.setEmergencyEvent(emergencyEvent);

			if (clusterModes != null)
				msg.setClusterModes(clusterModes);

			if (myKey != null)
				msg.setMyKey(myKey);

			if (electronicParkBrakeStatus != null)
				msg.setElectronicParkBrakeStatus(electronicParkBrakeStatus);

            if (engineOilLife != null)
                msg.setEngineOilLife(engineOilLife);

            if (fuelRange != null)
                msg.setFuelRange(fuelRange);
            
			return msg;
		}

		public static RequestNotifyMessage buildVehicleInfoOnVehicleData(GPSData gps, float? speed, int? rpm, float? fuelLevel,
                                                                 ComponentVolumeStatus? fuelLevel_State, float? instantFuelConsumption, float? externalTemperature, string vin,
                                                                 PRNDL? prndl, TurnSignal? turnSignal, TireStatus tirePressure, int? odometer, BeltStatus beltStatus, BodyInformation bodyInformation,
                                                                DeviceStatus deviceStatus, VehicleDataEventStatus? driverBraking, WiperStatus? wiperStatus, HeadLampStatus headLampStatus,
                                                                float? engineTorque, float? accPedalPosition, float? steeringWheelAngle, ECallInfo eCallInfo, AirbagStatus airbagStatus,
                                                                 EmergencyEvent emergencyEvent, ClusterModeStatus clusterModes, MyKey myKey, ElectronicParkBrakeStatus? electronicParkBrakeStatus,
                                                                         float? engineOilLife, List<FuelRange> fuelRange)
		{
            Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData msg = new Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData();

			if (gps != null)
				msg.setGps(gps);

			if (speed != null)
				msg.setSpeed(speed);

			if (rpm != null)
				msg.setRpm(rpm);

			if (fuelLevel != null)
				msg.setFuelLevel(fuelLevel);

			if (fuelLevel_State != null)
				msg.setFuelLevel_State(fuelLevel_State);

			if (instantFuelConsumption != null)
				msg.setInstantFuelConsumption(instantFuelConsumption);

			if (externalTemperature != null)
				msg.setExternalTemperature(externalTemperature);

			if (vin != null)
				msg.setVin(vin);

			if (prndl != null)
				msg.setPrndl(prndl);

            if (turnSignal != null)
				msg.setTurnSignal(turnSignal);

			if (tirePressure != null)
				msg.setTirePressure(tirePressure);

			if (odometer != null)
				msg.setOdometer(odometer);

			if (beltStatus != null)
				msg.setBeltStatus(beltStatus);

			if (bodyInformation != null)
				msg.setBodyInformation(bodyInformation);

			if (deviceStatus != null)
				msg.setDeviceStatus(deviceStatus);

			if (driverBraking != null)
				msg.setDriverBraking(driverBraking);

			if (wiperStatus != null)
				msg.setWiperStatus(wiperStatus);

			if (headLampStatus != null)
				msg.setHeadLampStatus(headLampStatus);

			if (engineTorque != null)
				msg.setEngineTorque(engineTorque);

			if (accPedalPosition != null)
				msg.setAccPedalPosition(accPedalPosition);

			if (steeringWheelAngle != null)
				msg.setSteeringWheelAngle(steeringWheelAngle);

			if (eCallInfo != null)
				msg.setECallInfo(eCallInfo);

			if (airbagStatus != null)
				msg.setAirbagStatus(airbagStatus);

			if (emergencyEvent != null)
				msg.setEmergencyEvent(emergencyEvent);

			if (clusterModes != null)
				msg.setClusterModes(clusterModes);

			if (myKey != null)
				msg.setMyKey(myKey);

            if (electronicParkBrakeStatus != null)
                msg.setElectronicParkBrakeStatus(electronicParkBrakeStatus);

            if (engineOilLife != null)
                msg.setEngineOilLife(engineOilLife);

            if (fuelRange != null)
                msg.setFuelRange(fuelRange);

			return msg;
		}

		public static RpcResponse buildVehicleInfoGetVehicleTypeResponse(int corrId, Common.Enums.Result? resultCode, VehicleType vehicleType)
		{
			Controllers.VehicleInfo.OutgoingResponses.GetVehicleType msg = new Controllers.VehicleInfo.OutgoingResponses.GetVehicleType();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            if (vehicleType != null)
                msg.setVehicleType(vehicleType);

			return msg;
		}

		public static RpcResponse buildVehicleInfoReadDIDResponse(int corrId, Common.Enums.Result? resultCode, List<DIDResult> didResult)
		{
			Controllers.VehicleInfo.OutgoingResponses.ReadDID msg = new Controllers.VehicleInfo.OutgoingResponses.ReadDID();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (didResult != null)
				msg.setDidResult(didResult);

			return msg;
		}

		public static RpcResponse buildVehicleInfoSubscribeVehicleDataResponse(int corrId, Common.Enums.Result? resultCode, VehicleDataResult gps, VehicleDataResult speed,
                                  VehicleDataResult rpm, VehicleDataResult fuelLevel, VehicleDataResult fuelLevel_State, VehicleDataResult instantFuelConsumption,
                                  VehicleDataResult externalTemperature, VehicleDataResult prndl, VehicleDataResult turnSignal, VehicleDataResult tirePressure, VehicleDataResult odometer,
                                  VehicleDataResult beltStatus, VehicleDataResult bodyInformation, VehicleDataResult deviceStatus, VehicleDataResult driverBraking,
                                  VehicleDataResult wiperStatus, VehicleDataResult headLampStatus, VehicleDataResult engineTorque, VehicleDataResult accPedalPosition,
                                  VehicleDataResult steeringWheelAngle, VehicleDataResult eCallInfo, VehicleDataResult airbagStatus, VehicleDataResult emergencyEvent,
                                  VehicleDataResult clusterModes, VehicleDataResult myKey, VehicleDataResult electronicParkBrakeStatus, VehicleDataResult engineOilLife,
                                  VehicleDataResult fuelRange)
		{
			Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData msg = new Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (gps != null)
                msg.setGps(gps);

			if (speed != null)
				msg.setSpeed(speed);

			if (rpm != null)
				msg.setRpm(rpm);

			if (fuelLevel != null)
				msg.setFuelLevel(fuelLevel);

			if (fuelLevel_State != null)
				msg.setFuelLevel_State(fuelLevel_State);

			if (instantFuelConsumption != null)
				msg.setInstantFuelConsumption(instantFuelConsumption);

			if (externalTemperature != null)
				msg.setExternalTemperature(externalTemperature);

			if (prndl != null)
				msg.setPrndl(prndl);

            if (turnSignal != null)
                msg.setTurnSignal(turnSignal);

			if (tirePressure != null)
				msg.setTirePressure(tirePressure);

			if (odometer != null)
				msg.setOdometer(odometer);

			if (beltStatus != null)
				msg.setBeltStatus(beltStatus);

			if (bodyInformation != null)
				msg.setBodyInformation(bodyInformation);

			if (deviceStatus != null)
				msg.setDeviceStatus(deviceStatus);

			if (driverBraking != null)
                msg.setdriverBraking(driverBraking);

			if (wiperStatus != null)
				msg.setWiperStatus(wiperStatus);

			if (headLampStatus != null)
				msg.setHeadLampStatus(headLampStatus);

			if (engineTorque != null)
				msg.setEngineTorque(engineTorque);

			if (accPedalPosition != null)
				msg.setAccPedalPosition(accPedalPosition);

			if (steeringWheelAngle != null)
				msg.setSteeringWheelAngle(steeringWheelAngle);

			if (eCallInfo != null)
				msg.setECallInfo(eCallInfo);

			if (airbagStatus != null)
				msg.setAirbagStatus(airbagStatus);

			if (emergencyEvent != null)
				msg.setEmergencyEvent(emergencyEvent);

			if (clusterModes != null)
				msg.setClusterModes(clusterModes);

			if (myKey != null)
				msg.setMyKey(myKey);
            
			if (electronicParkBrakeStatus != null)
				msg.setElectronicParkBrakeStatus(electronicParkBrakeStatus);

            if (engineOilLife != null)
                msg.setEngineOilLife(engineOilLife);

            if (fuelRange != null)
                msg.setFuelRange(fuelRange);

			return msg;
		}

		public static RpcResponse buildVehicleInfoUnsubscribeVehicleDataResponse(int corrId, Common.Enums.Result? resultCode, VehicleDataResult gps, VehicleDataResult speed,
								  VehicleDataResult rpm, VehicleDataResult fuelLevel, VehicleDataResult fuelLevel_State, VehicleDataResult instantFuelConsumption,
								  VehicleDataResult externalTemperature, VehicleDataResult prndl, VehicleDataResult turnSignal, VehicleDataResult tirePressure, VehicleDataResult odometer,
								  VehicleDataResult beltStatus, VehicleDataResult bodyInformation, VehicleDataResult deviceStatus, VehicleDataResult driverBraking,
								  VehicleDataResult wiperStatus, VehicleDataResult headLampStatus, VehicleDataResult engineTorque, VehicleDataResult accPedalPosition,
								  VehicleDataResult steeringWheelAngle, VehicleDataResult eCallInfo, VehicleDataResult airbagStatus, VehicleDataResult emergencyEvent,
                                  VehicleDataResult clusterModes, VehicleDataResult myKey, VehicleDataResult electronicParkBrakeStatus, VehicleDataResult engineOilLife,
                                  VehicleDataResult fuelRange)
		{
			Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData msg = new Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (gps != null)
				msg.setGps(gps);

			if (speed != null)
				msg.setSpeed(speed);

			if (rpm != null)
				msg.setRpm(rpm);

			if (fuelLevel != null)
				msg.setFuelLevel(fuelLevel);

			if (fuelLevel_State != null)
				msg.setFuelLevel_State(fuelLevel_State);

			if (instantFuelConsumption != null)
				msg.setInstantFuelConsumption(instantFuelConsumption);

			if (externalTemperature != null)
				msg.setExternalTemperature(externalTemperature);

			if (prndl != null)
				msg.setPrndl(prndl);

            if (turnSignal != null)
                msg.setTurnSignal(turnSignal);

			if (tirePressure != null)
				msg.setTirePressure(tirePressure);

			if (odometer != null)
				msg.setOdometer(odometer);

			if (beltStatus != null)
				msg.setBeltStatus(beltStatus);

			if (bodyInformation != null)
				msg.setBodyInformation(bodyInformation);

			if (deviceStatus != null)
				msg.setDeviceStatus(deviceStatus);

			if (driverBraking != null)
				msg.setdriverBraking(driverBraking);

			if (wiperStatus != null)
				msg.setWiperStatus(wiperStatus);

			if (headLampStatus != null)
				msg.setHeadLampStatus(headLampStatus);

			if (engineTorque != null)
				msg.setEngineTorque(engineTorque);

			if (accPedalPosition != null)
				msg.setAccPedalPosition(accPedalPosition);

			if (steeringWheelAngle != null)
				msg.setSteeringWheelAngle(steeringWheelAngle);

			if (eCallInfo != null)
				msg.setECallInfo(eCallInfo);

			if (airbagStatus != null)
				msg.setAirbagStatus(airbagStatus);

			if (emergencyEvent != null)
				msg.setEmergencyEvent(emergencyEvent);

			if (clusterModes != null)
				msg.setClusterModes(clusterModes);

			if (myKey != null)
				msg.setMyKey(myKey);

			if (electronicParkBrakeStatus != null)
				msg.setElectronicParkBrakeStatus(electronicParkBrakeStatus);

            if (engineOilLife != null)
                msg.setEngineOilLife(engineOilLife);

            if (fuelRange != null)
                msg.setFuelRange(fuelRange);

			return msg;
		}

		public static RpcResponse buildTTSChangeRegistrationResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.TTS.OutgoingResponses.ChangeRegistration msg = new Controllers.TTS.OutgoingResponses.ChangeRegistration();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildTTSGetCapabilitiesResponse(int corrId, Common.Enums.Result? resultCode, List<SpeechCapabilities> speechCapabilities, List<PrerecordedSpeech> prerecordedSpeechCapabilities)
		{
			Controllers.TTS.OutgoingResponses.GetCapabilities msg = new Controllers.TTS.OutgoingResponses.GetCapabilities();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (speechCapabilities != null)
				msg.setSpeechCapabilities(speechCapabilities);

			if (prerecordedSpeechCapabilities != null)
				msg.setPrerecordedSpeechCapabilities(prerecordedSpeechCapabilities);

			return msg;
		}

		public static RpcResponse buildTTSGetSupportedLanguagesResponse(int corrId, Common.Enums.Result? resultCode, List<Language> languages)
		{
			Controllers.TTS.OutgoingResponses.GetSupportedLanguages msg = new Controllers.TTS.OutgoingResponses.GetSupportedLanguages();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (languages != null)
				msg.setLanguages(languages);

			return msg;
		}

		public static RpcResponse buildTTSSetGlobalPropertiesResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.TTS.OutgoingResponses.SetGlobalProperties msg = new Controllers.TTS.OutgoingResponses.SetGlobalProperties();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavAlertManeuverResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.AlertManeuver msg = new Controllers.Navigation.OutgoingResponses.AlertManeuver();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

        public static RpcResponse buildNavSetVideoConfigResponse(int corrId, Common.Enums.Result? resultCode, List<string> rejectedParams)
        {
            Controllers.Navigation.OutgoingResponses.SetVideoConfig msg = new Controllers.Navigation.OutgoingResponses.SetVideoConfig();
            msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            if (rejectedParams != null)
                msg.setRejectedParams(rejectedParams);

            return msg;
        }

		public static RpcResponse buildNavGetWayPointsResponse(int corrId, Common.Enums.Result? resultCode, List<LocationDetails> wayPoints)
		{
			Controllers.Navigation.OutgoingResponses.GetWayPoints msg = new Controllers.Navigation.OutgoingResponses.GetWayPoints();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (wayPoints != null)
				msg.setWayPoints(wayPoints);

			return msg;
		}

		public static RpcResponse buildNavSendLocationResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.SendLocation msg = new Controllers.Navigation.OutgoingResponses.SendLocation();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavShowConstantTBTResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.ShowConstantTBT msg = new Controllers.Navigation.OutgoingResponses.ShowConstantTBT();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavStartAudioStreamResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.StartAudioStream msg = new Controllers.Navigation.OutgoingResponses.StartAudioStream();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavStartStreamResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.StartStream msg = new Controllers.Navigation.OutgoingResponses.StartStream();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavStopAudioStreamResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.StopAudioStream msg = new Controllers.Navigation.OutgoingResponses.StopAudioStream();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavStopStreamResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.StopStream msg = new Controllers.Navigation.OutgoingResponses.StopStream();
			msg.setId(corrId);
			
            if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavSubscribeWayPointsResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.SubscribeWayPoints msg = new Controllers.Navigation.OutgoingResponses.SubscribeWayPoints();
			msg.setId(corrId);
			
            if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavUnsubscribeWayPointsResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints msg = new Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildNavUpdateTurnListResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.Navigation.OutgoingResponses.UpdateTurnList msg = new Controllers.Navigation.OutgoingResponses.UpdateTurnList();
			msg.setId(corrId);
			
            if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildBasicCommunicationAllowDeviceToConnectResponse(int corrId, Common.Enums.Result? resultCode, bool? allow)
		{
			Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect msg = new Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect();
			msg.setId(corrId);
            
            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			if (allow != null)
				msg.setAllow(allow);

			return msg;
		}

		public static RpcResponse buildBasicCommunicationDialNumberResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.DialNumber msg = new Controllers.BasicCommunication.OutgoingResponses.DialNumber();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildBasicCommunicationDecryptCertificateResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate msg = new Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildBasicCommunicationGetSystemInfoResponse(int corrId, Common.Enums.Result? resultCode, String ccpu_version, Language? language, String wersCountryCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo msg = new Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo();
			msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            if (language != null)
                msg.setLanguage((Language)language);

			if (ccpu_version != null)
				msg.setCcpuVersion(ccpu_version);

			if (wersCountryCode != null)
				msg.setWersCountryCode(wersCountryCode);

			return msg;
		}

        public static RpcResponse buildBasicCommunicationGetSystemTimeResponse(int corrId, Common.Enums.Result? resultCode, Common.Structs.DateTime systemTime)
        {
            Controllers.BasicCommunication.OutgoingResponses.GetSystemTime msg = new Controllers.BasicCommunication.OutgoingResponses.GetSystemTime();
            msg.setId(corrId);

            if (resultCode != null)
                msg.setResultCode((Common.Enums.Result)resultCode);

            if (systemTime != null)
                msg.setSystemTime(systemTime);

            return msg;
        }

		public static RpcResponse buildBasicCommunicationPolicyUpdateResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate msg = new Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildBasicCommunicationSystemRequestResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.SystemRequest msg = new Controllers.BasicCommunication.OutgoingResponses.SystemRequest();
			msg.setId(corrId);
			
            if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildBasicCommunicationUpdateAppListResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.UpdateAppList msg = new Controllers.BasicCommunication.OutgoingResponses.UpdateAppList();
			msg.setId(corrId);

			if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

		public static RpcResponse buildBasicCommunicationUpdateDeviceListResponse(int corrId, Common.Enums.Result? resultCode)
		{
			Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList msg = new Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList();
			msg.setId(corrId);
			
            if (resultCode != null)
				msg.setResultCode((Common.Enums.Result)resultCode);

			return msg;
		}

	}
}