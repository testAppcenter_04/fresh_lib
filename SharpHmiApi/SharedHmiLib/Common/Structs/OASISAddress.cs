﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class OASISAddress : RpcStruct
	{
		public string countryName;
		public string countryCode;
		public string postalCode;
		public string administrativeArea;
		public string subAdministrativeArea;
		public string locality;
		public string subLocality;
		public string thoroughfare;
		public string subThoroughfare;

		public OASISAddress()
		{
		}

		public string getCountryName()
		{
			return countryName;
		}

		public string getCountryCode()
		{
			return countryCode;		}

		public string getPostalCode()
		{
			return postalCode;		}

		public string getAdministrativeArea()
		{
			return administrativeArea;		}

		public string getSubAdministrativeArea()
		{
			return subAdministrativeArea;		}

		public string getLocality()
		{
			return locality;		}

		public string getSubLocality()
		{
			return subLocality;
		}

		public string getThoroughfare()
		{
			return thoroughfare;
		}

		public string getSubThoroughfare()
		{
			return subThoroughfare;
		}

	}
}