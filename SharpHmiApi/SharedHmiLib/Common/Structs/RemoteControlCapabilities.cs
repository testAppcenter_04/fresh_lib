﻿using System.Collections.Generic;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class RemoteControlCapabilities : RpcStruct
	{
		public List<ClimateControlCapabilities> climateControlCapabilities;
		public List<RadioControlCapabilities> radioControlCapabilities;
		public List<ButtonCapabilities> buttonCapabilities;
        public List<SeatControlCapabilities> seatControlCapabilities;
        public List<AudioControlCapabilities> audioControlCapabilities;
        public HMISettingsControlCapabilities hmiSettingsControlCapabilities;
        public LightControlCapabilities lightControlCapabilities;

		public RemoteControlCapabilities()
		{
		}

		public List<ClimateControlCapabilities> getClimateControlCapabilities()
		{
			return climateControlCapabilities;
		}

		public List<RadioControlCapabilities> getRadioControlCapabilities()
		{
			return radioControlCapabilities;		}

		public List<ButtonCapabilities> getButtonCapabilities()
		{
			return buttonCapabilities;		}

		public List<SeatControlCapabilities> getSeatControlCapabilities()
		{
			return seatControlCapabilities;
		}

		public List<AudioControlCapabilities> getAudioControlCapabilities()
		{
			return audioControlCapabilities;
		}

		public HMISettingsControlCapabilities getHmiSettingsControlCapabilities()
		{
			return hmiSettingsControlCapabilities;
		}

		public LightControlCapabilities getLightControlCapabilities()
		{
			return lightControlCapabilities;
		}
	}
}
