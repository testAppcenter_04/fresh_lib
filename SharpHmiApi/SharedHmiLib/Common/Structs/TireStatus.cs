﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class TireStatus : RpcStruct
	{
		public WarningLightStatus? pressureTelltale;
		public SingleTireStatus leftFront;
		public SingleTireStatus rightFront;
		public SingleTireStatus leftRear;
		public SingleTireStatus rightRear;
		public SingleTireStatus innerLeftRear;
		public SingleTireStatus innerRightRear;

		public TireStatus()
		{
		}

		public WarningLightStatus? getPressureTelltale()
		{
			return pressureTelltale;
		}

		public SingleTireStatus getleftFront()
		{
			return leftFront;
		}

		public SingleTireStatus getRightFront()
		{
			return rightFront;		}

		public SingleTireStatus getLeftRear()
		{
			return leftRear;		}

		public SingleTireStatus getRightRear()
		{
			return rightRear;		}

		public SingleTireStatus getInnerLeftRear()
		{
			return innerLeftRear;		}

		public SingleTireStatus getInnerRightRear()
		{
			return innerRightRear;
		}
	}
}