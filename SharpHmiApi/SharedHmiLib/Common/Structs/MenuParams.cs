﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class MenuParams : RpcStruct
	{
		public int? parentID;
		public int? position;
		public String menuName;

		public MenuParams()
		{
		}

		public int? getParentID()
		{
			return parentID;
		}

		public int? getPosition()
		{
			return position;		}

		public String getMenuName()
		{
			return menuName;		}

	}
}