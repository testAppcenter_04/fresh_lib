﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class HMIApplication : RpcStruct
	{
		public String appName;
		public String ngnMediaScreenAppName;
		public String icon;
		public DeviceInfo deviceInfo;
        public DeviceInfo secondaryDeviceInfo;
        public String policyAppID;
		public List<TTSChunk> ttsName;
		public List<String> vrSynonyms;
		public int? appID;
		public int? windowID;

		public Language? hmiDisplayLanguageDesired;
		public Boolean? isMediaApplication;
		public List<AppHMIType> appType;
		public Boolean? greyOut;
		public List<RequestType> requestType;
        public List<String> requestSubType;

		public HMIApplication()
		{
		}

		public String getAppName()
		{
			return appName;
		}

		public String getNgnMediaScreenAppName()
		{
			return ngnMediaScreenAppName;		}

		public String getIcon()
		{
			return icon;		}

		public DeviceInfo getDeviceInfo()
		{
			return deviceInfo;		}

        public DeviceInfo getSecondaryDeviceInfo()
        {
            return secondaryDeviceInfo;
        }

        public String getPolicyAppID()
		{
			return policyAppID;		}

		public List<TTSChunk> getTtsName()
		{
			return ttsName;		}

		public List<String> getVrSynonyms()
		{
			return vrSynonyms;		}

		public int? getAppID()
		{
			return appID;		}
		public int? getWindowID()
		{
			return windowID;		}
		public Language? getHmiDisplayLanguageDesired()
		{
			return hmiDisplayLanguageDesired;		}

		public Boolean? getIsMediaApplication()
		{
			return isMediaApplication;		}

		public List<AppHMIType> getAppType()
		{
			return appType;		}

		public Boolean? getGreyOut()
		{
			return greyOut;		}

		public List<RequestType> getRequestType()
		{
			return requestType;		}

        public List<String> getRequestSubType()
        {
            return requestSubType;
        }
	}
}
