﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class VrHelpItem : RpcStruct
	{
		public string text;
		public Image image;
		public int? position;

		public VrHelpItem()
		{
		}

		public string getText()
		{
			return text;
		}

		public Image getImage()
		{
			return image;
		}

		public int? getPosition()
		{
			return position;
		}

	}
}
