﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class Grid : RpcStruct
	{
		public int? col;
		public int? row;
		public int? level;
		public int? colspan;
		public int? rowSpan;
		public int? levelSpan;


		public Grid()
		{
		}

		public int? getCol()
		{
			return col;
		}
		public int? getRow()
		{
			return row;
		}
		public int? getLevel()
		{
			return level;
		}
		public int? getColSpan()
		{
			return colspan;
		}
		public int? getRowSpan()
		{
			return rowSpan;
		}
		public int? getLevelSpan()
		{
			return levelSpan;
		}
	}
}
