﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class ButtonCapabilities : RpcStruct
	{
		public ButtonName? name;
		public Boolean? shortPressAvailable;
		public Boolean? longPressAvailable;
		public Boolean? upDownAvailable;
		//public ModuleInfo moduleInfo;
		public ButtonCapabilities()
		{
		}

		public ButtonName? getName()
		{
			return name;
		}
		/*public ModuleInfo getModuleInfo()
		{
			return moduleInfo;
		}*/
		public Boolean? getShortPressAvailable()
		{
			return shortPressAvailable;		}

		public Boolean? getLongPressAvailable()
		{
			return longPressAvailable;		}

		public Boolean? getUpDownAvailable()
		{
			return upDownAvailable;		}

	}
}
