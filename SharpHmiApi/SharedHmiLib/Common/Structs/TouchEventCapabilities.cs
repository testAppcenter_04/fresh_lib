﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class TouchEventCapabilities : RpcStruct
	{
		public Boolean? pressAvailable;
		public Boolean? multiTouchAvailable;
		public Boolean? doublePressAvailable;

		public TouchEventCapabilities()
		{
		}

		public Boolean? getPressAvailable()
		{
			return pressAvailable;
		}

		public Boolean? getMultiTouchAvailable()
		{
			return multiTouchAvailable;		}

		public Boolean? getDoublePressAvailable()
		{
			return doublePressAvailable;		}

	}
}