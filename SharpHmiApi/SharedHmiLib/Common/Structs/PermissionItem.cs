﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class PermissionItem : RpcStruct
	{
		public String name;
		public int? id;
		public Boolean? allowed;
	
		public PermissionItem()
		{
		}

		public String getName()
		{
			return name;
		}

		public int? getId()
		{
			return id;		}

		public Boolean? getAllowed()
		{
			return allowed;		}

	}
}
