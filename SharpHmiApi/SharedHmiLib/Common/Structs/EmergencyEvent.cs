﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class EmergencyEvent : RpcStruct
	{
		public EmergencyEventType? emergencyEventType;
		public FuelCutoffStatus? fuelCutoffStatus;
		public VehicleDataEventStatus? rolloverEvent;
		public VehicleDataEventStatus? maximumChangeVelocity;
		public VehicleDataEventStatus? multipleEvents;

		public EmergencyEvent()
		{
		}

		public EmergencyEventType? getEmergencyEventType()
		{
			return emergencyEventType;
		}

		public FuelCutoffStatus? getFuelCutoffStatus()
		{
			return fuelCutoffStatus;		}

		public VehicleDataEventStatus? getRolloverEvent()
		{
			return rolloverEvent;		}

		public VehicleDataEventStatus? getMaximumChangeVelocity()
		{
			return maximumChangeVelocity;		}

		public VehicleDataEventStatus? getMultipleEvents()
		{
			return multipleEvents;		}
	}
}