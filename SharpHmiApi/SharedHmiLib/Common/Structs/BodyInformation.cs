﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class BodyInformation : RpcStruct
	{
		public Boolean? parkBrakeActive;
		public IgnitionStableStatus? ignitionStableStatus;
		public IgnitionStatus? ignitionStatus;
		public Boolean? driverDoorAjar;
		public Boolean? passengerDoorAjar;
		public Boolean? rearLeftDoorAjar;
		public Boolean? rearRightDoorAjar;

		public BodyInformation()
		{
		}

		public Boolean? getParkBrakeActive()
		{
			return parkBrakeActive;
		}

		public IgnitionStableStatus? getIgnitionStableStatus()
		{
			return ignitionStableStatus;		}

		public IgnitionStatus? getIgnitionStatus()
		{
			return ignitionStatus;		}

		public Boolean? getDriverDoorAjar()
		{
			return driverDoorAjar;		}

		public Boolean? getPassengerDoorAjar()
		{
			return passengerDoorAjar;		}

		public Boolean? getRearLeftDoorAjar()
		{
			return rearLeftDoorAjar;		}

		public Boolean? getRearRightDoorAjar()
		{
			return rearRightDoorAjar;		}

	}
}