﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class TTSChunk : RpcStruct
	{
		public string text;
		public SpeechCapabilities? type;

		public TTSChunk()
		{
		}

		public string getText()
		{
			return text;
		}

		public SpeechCapabilities? getType()
		{
			return type;		}

	}
}