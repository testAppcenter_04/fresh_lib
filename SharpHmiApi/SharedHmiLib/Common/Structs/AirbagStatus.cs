﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class AirbagStatus : RpcStruct
	{
		public VehicleDataEventStatus? driverAirbagDeployed;
		public VehicleDataEventStatus? driverSideAirbagDeployed;
		public VehicleDataEventStatus? driverCurtainAirbagDeployed;
		public VehicleDataEventStatus? passengerAirbagDeployed;
		public VehicleDataEventStatus? passengerCurtainAirbagDeployed;
		public VehicleDataEventStatus? driverKneeAirbagDeployed;
		public VehicleDataEventStatus? passengerSideAirbagDeployed;
		public VehicleDataEventStatus? passengerKneeAirbagDeployed;


		public AirbagStatus()
		{
		}

		public VehicleDataEventStatus? getDriverAirbagDeployed()
		{
			return driverAirbagDeployed;
		}

		public VehicleDataEventStatus? getDriverSideAirbagDeployed()
		{
			return driverSideAirbagDeployed;		}

		public VehicleDataEventStatus? getDriverCurtainAirbagDeployed()
		{
			return driverCurtainAirbagDeployed;
		}

		public VehicleDataEventStatus? getPassengerAirbagDeployed()
		{
			return passengerAirbagDeployed;		}

		public VehicleDataEventStatus? getPassengerCurtainAirbagDeployed()
		{
			return passengerCurtainAirbagDeployed;		}

		public VehicleDataEventStatus? getDriverKneeAirbagDeployed()
		{
			return driverKneeAirbagDeployed;		}

		public VehicleDataEventStatus? getPassengerSideAirbagDeployed()
		{
			return passengerSideAirbagDeployed;		}

		public VehicleDataEventStatus? getPassengerKneeAirbagDeployed()
		{
			return passengerKneeAirbagDeployed;		}

	}
}