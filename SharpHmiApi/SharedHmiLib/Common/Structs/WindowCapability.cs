﻿using System;
using System.Collections.Generic;

using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class WindowCapability: RpcStruct
	{
		public List<ImageField> imageFields;
		public List<TextField> textFields;
		public int? windowID;
		public List<ImageType> imageTypeSupported;
		public List<String> templatesAvailable;
		public int? numCustomPresetsAvailable;
		public List<ButtonCapabilities> buttonCapabilities;
		public List<SoftButtonCapabilities> softButtonCapabilities;

		public WindowCapability()
		{
		}
		
		public int? getWindowID()
		{
			return windowID;		}
		public List<String> getTemplatesAvailable()
		{
			return templatesAvailable;		}
		public int? getNumCustomPresetsAvailable()
		{
			return numCustomPresetsAvailable;		}
		public List<ImageType> getImageTypeSupported()
		{
			return imageTypeSupported;		}
		public List<TextField> getTextFields()
		{
			return textFields;		}
		public List<ImageField> getImageFields()
		{
			return imageFields;		}
		public List<ButtonCapabilities> getButtonCapabilities()
		{
			return buttonCapabilities;		}
		public List<SoftButtonCapabilities> getSoftButtonCapabilities()
		{
			return softButtonCapabilities;		}
	}
}
