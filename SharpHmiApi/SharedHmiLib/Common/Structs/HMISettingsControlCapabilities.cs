﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class HMISettingsControlCapabilities : RpcStruct
	{
		public String moduleName;
		public Boolean? distanceUnitAvailable;
        public Boolean? temperatureUnitAvailable;
        public Boolean? displayModeUnitAvailable;
		public ModuleInfo moduleInfo;
		public HMISettingsControlCapabilities()
		{
		}

		public String getModuleName()
		{
			return moduleName;
		}

		public ModuleInfo getModuleInfo()
		{
			return moduleInfo;
		}

		public Boolean? getDistanceUnitAvailable()
		{
			return distanceUnitAvailable;		}

		public Boolean? getTemperatureUnitAvailable()
		{
			return temperatureUnitAvailable;
		}

		public Boolean? getDisplayModeUnitAvailable()
		{
			return displayModeUnitAvailable;
		}
	}
}
