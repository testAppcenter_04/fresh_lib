﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class SeatControlCapabilities : RpcStruct
	{
		public String moduleName;
        public Boolean? heatingEnabledAvailable;
		public Boolean? coolingEnabledAvailable;
        public Boolean? heatingLevelAvailable;
        public Boolean? coolingLevelAvailable;
        public Boolean? horizontalPositionAvailable;
        public Boolean? verticalPositionAvailable;
        public Boolean? frontVerticalPositionAvailable;
        public Boolean? backVerticalPositionAvailable;
        public Boolean? backTiltAngleAvailable;
        public Boolean? headSupportHorizontalPositionAvailable;
        public Boolean? headSupportVerticalPositionAvailable;
        public Boolean? massageEnabledAvailable;
        public Boolean? massageModeAvailable;
        public Boolean? massageCushionFirmnessAvailable;
        public Boolean? memoryAvailable;
		public ModuleInfo moduleInfo;

		public SeatControlCapabilities()
		{
		}

		public String getModuleName()
		{
			return moduleName;		}

		public ModuleInfo getModuleInfo()
		{
			return moduleInfo;
		}
		public Boolean? getHeatingEnabledAvailable()
		{
			return heatingEnabledAvailable;
		}

		public Boolean? getCoolingEnabledAvailable()
		{
			return coolingEnabledAvailable;
		}

		public Boolean? getHeatingLevelAvailable()
		{
			return heatingLevelAvailable;
		}

		public Boolean? getCoolingLevelAvailable()
		{
			return coolingLevelAvailable;
		}

		public Boolean? getHorizontalPositionAvailable()
		{
			return horizontalPositionAvailable;
		}

		public Boolean? getVerticalPositionAvailable()
		{
			return verticalPositionAvailable;
		}

		public Boolean? getFrontVerticalPositionAvailable()
		{
			return frontVerticalPositionAvailable;
		}

		public Boolean? getBackVerticalPositionAvailable()
		{
			return backVerticalPositionAvailable;
		}

		public Boolean? getBackTiltAngleAvailable()
		{
			return backTiltAngleAvailable;
		}

		public Boolean? getHeadSupportHorizontalPositionAvailable()
		{
			return headSupportHorizontalPositionAvailable;
		}

		public Boolean? getHeadSupportVerticalPositionAvailable()
		{
			return headSupportVerticalPositionAvailable;
		}

		public Boolean? getMassageEnabledAvailable()
		{
			return massageEnabledAvailable;
		}

		public Boolean? getMassageModeAvailable()
		{
			return massageModeAvailable;
		}

		public Boolean? getMassageCushionFirmnessAvailable()
		{
			return massageCushionFirmnessAvailable;
		}

		public Boolean? getMemoryAvailable()
		{
			return memoryAvailable;
		}
	}
}