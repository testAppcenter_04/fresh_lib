﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class ServiceInfo : RpcStruct
	{
		public string url;
		public int? appID;

		public ServiceInfo()
		{
		}

		public string getUrl()
		{
			return url;
		}

		public int? getAppID()
		{
			return appID;		}

	}
}
