﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class ClusterModeStatus : RpcStruct
	{
		public Boolean? powerModeActive;
		public PowerModeQualificationStatus? powerModeQualificationStatus;
		public CarModeStatus? carModeStatus;
		public PowerModeStatus? powerModeStatus;

		public ClusterModeStatus()
		{
		}

		public Boolean? getPowerModeActive()
		{
			return powerModeActive;
		}

		public PowerModeQualificationStatus? getPowerModeQualificationStatus()
		{
			return powerModeQualificationStatus;		}

		public CarModeStatus? getCarModeStatus()
		{
			return carModeStatus;		}

		public PowerModeStatus? getPowerModeStatus()
		{
			return powerModeStatus;		}

	}
}