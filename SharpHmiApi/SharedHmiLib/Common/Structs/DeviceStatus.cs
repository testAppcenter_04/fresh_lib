﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class DeviceStatus : RpcStruct
	{
		public Boolean? voiceRecOn;
		public Boolean? btIconOn;
		public Boolean? callActive;
		public Boolean? phoneRoaming;
		public Boolean? textMsgAvailable;
		public DeviceLevelStatus? battLevelStatus;
		public Boolean? stereoAudioOutputMuted;
		public Boolean? monoAudioOutputMuted;
		public DeviceLevelStatus? signalLevelStatus;
		public PrimaryAudioSource? primaryAudioSource;
		public Boolean? eCallEventActive;

		public DeviceStatus()
		{
		}

		public Boolean? getVoiceRecOn()
		{
			return voiceRecOn;
		}

		public Boolean? getBtIconOn()
		{
			return btIconOn;		}

		public Boolean? getCallActive()
		{
			return callActive;		}

		public Boolean? getPhoneRoaming()
		{
			return phoneRoaming;		}

		public Boolean? getTextMsgAvailable()
		{
			return textMsgAvailable;		}

		public DeviceLevelStatus? getBattLevelStatus()
		{
			return battLevelStatus;		}

		public Boolean? getStereoAudioOutputMuted()
		{
			return stereoAudioOutputMuted;		}

		public Boolean? getMonoAudioOutputMuted()
		{
			return monoAudioOutputMuted;		}

		public DeviceLevelStatus? getSignalLevelStatus()
		{
			return signalLevelStatus;		}

		public PrimaryAudioSource? getPrimaryAudioSource()
		{
			return primaryAudioSource;		}

		public Boolean? getECallEventActive()
		{
			return eCallEventActive;		}

	}
}