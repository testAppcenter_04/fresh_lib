﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class MyKey : RpcStruct
	{
		public VehicleDataStatus? e911Override;

		public MyKey()
		{
		}

		public VehicleDataStatus? getE911Override()
		{
			return e911Override;
		}
	}
}