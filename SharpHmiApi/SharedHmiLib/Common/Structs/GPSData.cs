﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class GPSData : RpcStruct
	{
		public float? longitudeDegrees;
		public float? latitudeDegrees;
		public int? utcYear;
		public int? utcMonth;
		public int? utcDay;
		public int? utcHours;
		public int? utcMinutes;
		public int? utcSeconds;
		public CompassDirection? compassDirection;
		public float? pdop;
		public float? hdop;
		public float? vdop;
		public Boolean? actual;
		public int? satellites;
		public Dimension? dimension;
		public float? altitude;
		public float? heading;
		public float? speed;
		public bool? shifted;

		public GPSData()
		{
		}

		public float? getLongitudeDegrees()
		{
			return longitudeDegrees;
		}

		public float? getLatitudeDegrees()
		{
			return latitudeDegrees;		}
		public bool? getShifted()
		{
			return shifted;		}
		public int? getUtcYear()
		{
			return utcYear;		}

		public int? getUtcMonth()
		{
			return utcMonth;		}

		public int? getUtcDay()
		{
			return utcDay;		}

		public int? getUtcHours()
		{
			return utcHours;		}

		public int? getUtcMinutes()
		{
			return utcMinutes;		}

		public int? getUtcSeconds()
		{
			return utcSeconds;		}

		public CompassDirection? getCompassDirection()
		{
			return compassDirection;		}

		public float? getPdop()
		{
			return pdop;		}

		public float? getHdop()
		{
			return hdop;		}

		public float? getVdop()
		{
			return vdop;		}

		public Boolean? getActual()
		{
			return actual;		}

		public int? getSatellites()
		{
			return satellites;		}

		public Dimension? getDimension()
		{
			return dimension;		}

		public float? getAltitude()
		{
			return altitude;		}

		public float? getHeading()
		{
			return heading;		}

		public float? getSpeed()
		{
			return speed;		}

	}
}