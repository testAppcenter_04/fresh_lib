﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class SeatLocation : RpcStruct
	{
		public Grid grid;
		public SeatLocation()
		{

		}
		public Grid getGrid()
		{
			return grid;
		}
	}
}
