﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class SeatMemoryAction : RpcStruct
	{
		public int? id;
        public String label;
		public SeatMemoryActionType? action;

		public SeatMemoryAction()
		{
		}

		public int? getId()
		{
			return id;		}

		public String getLabel()
		{
			return label;
		}

		public SeatMemoryActionType? getAction()
		{
			return action;
		}
	}
}