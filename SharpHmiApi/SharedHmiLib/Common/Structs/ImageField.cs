﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class ImageField : RpcStruct
	{
		public ImageFieldName? name;
		public List<FileType> imageTypeSupported;
		public ImageResolution imageResolution;

		public ImageField()
		{
		}

		public ImageFieldName? getName()
		{
			return name;
		}

		public List<FileType> getImageTypeSupported()
		{
			return imageTypeSupported;		}

		public ImageResolution getImageResolution()
		{
			return imageResolution;		}

	}
}