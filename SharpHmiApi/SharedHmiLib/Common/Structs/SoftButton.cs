﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class SoftButton : RpcStruct
	{
		public SoftButtonType? type;
		public string text;
		public Image image;
		public Boolean? isHighlighted;
		public int? softButtonID;
		public SystemAction? systemAction;

		public SoftButton()
		{
		}

		public SoftButtonType? getType()
		{
			return type;
		}

		public string getText()
		{
			return text;
		}

		public Image getImage()
		{
			return image;
		}

		public Boolean? getIsHighlighted()
		{
			return isHighlighted;
		}

		public int? getSoftButtonID()
		{
			return softButtonID;
		}

		public SystemAction? getSystemAction()
		{
			return systemAction;
		}

	}
}
