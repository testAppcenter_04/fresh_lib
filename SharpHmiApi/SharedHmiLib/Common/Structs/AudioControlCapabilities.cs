﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class AudioControlCapabilities : RpcStruct
	{
		public String moduleName;
        public Boolean? sourceAvailable;
		public Boolean? volumeAvailable;
        public Boolean? equalizerAvailable;
        public int? equalizerMaxChannelId;
		public ModuleInfo moduleInfo;
		public AudioControlCapabilities()
		{
		}

		public String getModuleName()
		{
			return moduleName;		}

		public ModuleInfo getModuleInfo()
		{
			return moduleInfo;
		}
		public Boolean? getSourceAvailable()
		{
			return sourceAvailable;
		}

		public Boolean? getVolumeAvailable()
		{
			return volumeAvailable;
		}

		public Boolean? getEqualizerAvailable()
		{
			return equalizerAvailable;
		}

		public int? getEqualizerMaxChannelId()
		{
			return equalizerMaxChannelId;
		}
	}
}