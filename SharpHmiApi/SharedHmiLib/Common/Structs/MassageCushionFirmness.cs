﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class MassageCushionFirmness : RpcStruct
	{
		public MassageCushion? cushion;
		public int? firmness;

		public MassageCushionFirmness()
		{
		}

		public MassageCushion? getMassageCushion()
		{
			return cushion;
		}

		public int? getFirmness()
		{
			return firmness;		}
	}
}