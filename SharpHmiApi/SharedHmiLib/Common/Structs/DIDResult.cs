﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class DIDResult : RpcStruct
	{
		public VehicleDataResultCode? resultCode;
		public int? didLocation;
		public string data;

		public DIDResult()
		{
		}

		public VehicleDataResultCode? getResultCode()
		{
			return resultCode;
		}

		public int? getDidLocation()
		{
			return didLocation;		}

		public string getData()
		{
			return data;		}

	}
}