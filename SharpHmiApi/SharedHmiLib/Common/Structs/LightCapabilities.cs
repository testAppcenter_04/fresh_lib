﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class LightCapabilities : RpcStruct
	{
		public LightName? name;
        public bool? densityAvailable;
        public bool? rgbColorSpaceAvailable;
        public bool? statusAvailable;

		public LightCapabilities()
		{
		}

		public LightName? getName()
		{
			return name;
		}

		public bool? getDensityAvailable()
		{
			return densityAvailable;
		}

		public bool? getRGBColorSpaceAvailable()
		{
            return rgbColorSpaceAvailable;
		}

        public bool? getStatusAvailable()
        {
            return statusAvailable;
        }
	}
}