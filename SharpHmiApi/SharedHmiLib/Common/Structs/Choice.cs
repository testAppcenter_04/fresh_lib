﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class Choice : RpcStruct
	{
		public int? choiceID;
		public string menuName;
		public Image image;
		public string secondaryText;
		public string tertiaryText;
		public Image secondaryImage;

		public Choice()
		{
		}

		public int? getChoiceID()
		{
			return choiceID;
		}

		public string getMenuName()
		{
			return menuName;		}

		public Image getImage()
		{
			return image;		}

		public string getSecondaryText()
		{
			return secondaryText;		}

		public string getTertiaryText()
		{
			return tertiaryText;		}

		public Image getSecondaryImage()
		{
			return secondaryImage;		}

	}
}
