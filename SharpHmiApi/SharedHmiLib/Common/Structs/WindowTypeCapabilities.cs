﻿
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class WindowTypeCapabilities : RpcStruct
	{
		
		public WindowType? type;
		public int? maxNumberOfWindows;
		
		public WindowTypeCapabilities()
		{
		}
		public WindowType? getType()
		{
			return type;		}
		public int? getMaxNumberOfWindows()
		{
			return maxNumberOfWindows;		}

	}
}