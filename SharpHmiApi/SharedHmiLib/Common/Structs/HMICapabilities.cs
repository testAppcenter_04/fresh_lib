﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class HMICapabilities : RpcStruct
	{
		public Boolean? navigation;
		public Boolean? phoneCall;
        public Boolean? videoStreaming;

        public HMICapabilities()
		{
		}

		public Boolean? getNavigation()
		{
			return navigation;
		}

		public Boolean? getPhoneCall()
		{
			return phoneCall;
		}

        public Boolean? getVideoStreaming()
        {
            return videoStreaming;
        }
    }
}