﻿
using HmiApiLib.Base;
using System.Collections.Generic;

namespace HmiApiLib.Common.Structs
{
	public class DisplayCapability: RpcStruct
	{
		
		public string displayName;
		public WindowTypeCapabilities windowTypeSupported;
		public List<WindowCapability> windowCapabilities;
		public DisplayCapability()
		{
		}

		
		public string getDisplayName()
		{
			return displayName;
		}
		public WindowTypeCapabilities getWindowTypeSupported()
		{
			return windowTypeSupported;
		}
		public List<WindowCapability> getWindowCapabilities()
		{
			return windowCapabilities;
		}
		

	}
}