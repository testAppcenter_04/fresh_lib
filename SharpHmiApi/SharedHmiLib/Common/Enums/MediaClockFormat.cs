﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum MediaClockFormat
	{
		CLOCK1,
		CLOCK2,
		CLOCK3,
		CLOCKTEXT1,
		CLOCKTEXT2,
		CLOCKTEXT3,
		CLOCKTEXT4
	}
}