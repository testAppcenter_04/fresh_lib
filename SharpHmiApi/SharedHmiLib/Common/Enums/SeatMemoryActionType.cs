﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum SeatMemoryActionType
	{
		SAVE,
		RESTORE,
        NONE
	}
}
