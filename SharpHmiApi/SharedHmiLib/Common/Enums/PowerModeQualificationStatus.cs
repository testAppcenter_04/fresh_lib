﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum PowerModeQualificationStatus
	{
		POWER_MODE_UNDEFINED,
		POWER_MODE_EVALUATION_IN_PROGRESS,
		NOT_DEFINED,
		POWER_MODE_OK
	}
}