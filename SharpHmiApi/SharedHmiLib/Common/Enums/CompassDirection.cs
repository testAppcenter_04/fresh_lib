﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum CompassDirection
	{
		NORTH,
		NORTHWEST,
		WEST,
		SOUTHWEST,
		SOUTH,
		SOUTHEAST,
		EAST,
		NORTHEAST
	}
}