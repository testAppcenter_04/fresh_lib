﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace HmiApiLib.Common.Enums
{
	[JsonConverter(typeof(StringEnumConverter))]
	public enum Language
	{
		[EnumMember(Value = "EN-US")]
		EN_US,
		[EnumMember(Value = "ES-MX")]
		ES_MX,
		[EnumMember(Value = "FR-CA")]
		FR_CA,
		[EnumMember(Value = "DE-DE")]
		DE_DE,
		[EnumMember(Value = "ES-ES")]
		ES_ES,
		[EnumMember(Value = "EN-GB")]
		EN_GB,
		[EnumMember(Value = "RU-RU")]
		RU_RU,
		[EnumMember(Value = "TR-TR")]
		TR_TR,
		[EnumMember(Value = "PL-PL")]
		PL_PL,
		[EnumMember(Value = "FR-FR")]
		FR_FR,
		[EnumMember(Value = "IT-IT")]
		IT_IT,
		[EnumMember(Value = "SV-SE")]
		SV_SE,
		[EnumMember(Value = "PT-PT")]
		PT_PT,
		[EnumMember(Value = "NL-NL")]		
		NL_NL,
		[EnumMember(Value = "EN-AU")]		
		EN_AU,
		[EnumMember(Value = "ZH-CN")]		
		ZH_CN,
		[EnumMember(Value = "ZH-TW")]
		ZH_TW,
		[EnumMember(Value = "JA-JP")]
		JA_JP,
		[EnumMember(Value = "AR-SA")]
		AR_SA,
		[EnumMember(Value = "KO-KR")]
		KO_KR,
		[EnumMember(Value = "PT-BR")]
		PT_BR,
		[EnumMember(Value = "CS-CZ")]
		CS_CZ,
		[EnumMember(Value = "DA-DK")]
		DA_DK,
		[EnumMember(Value = "NO-NO")]
		NO_NO,
		[EnumMember(Value = "NL-BE")]
		NL_BE,
		[EnumMember(Value = "EL-GR")]
		EL_GR,
		[EnumMember(Value = "HU-HU")]
		HU_HU,
		[EnumMember(Value = "FI-FI")]
		FI_FI,
		[EnumMember(Value = "SK-SK")]
		SK_SK
	}
}
