﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum DisplayType
	{
		CID,
		TYPE2,
		TYPE5,
		NGN,
		GEN2_8_DMA,
		GEN2_6_DMA,
		MFD3,
		MFD4,
		MFD5,
		GEN3_8_INCH,
		SDL_GENERIC
	}
}
