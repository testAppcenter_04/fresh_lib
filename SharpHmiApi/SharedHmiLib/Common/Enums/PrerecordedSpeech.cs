﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum PrerecordedSpeech
	{
		HELP_JINGLE,
		INITIAL_JINGLE,
		LISTEN_JINGLE,
		POSITIVE_JINGLE,
		NEGATIVE_JINGLE
	}
}
