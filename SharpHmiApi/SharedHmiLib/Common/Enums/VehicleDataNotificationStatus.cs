﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum VehicleDataNotificationStatus
	{
		NOT_SUPPORTED,
		NORMAL,
		ACTIVE,
		NOT_USED
	}
}