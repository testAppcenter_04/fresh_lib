﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum AmbientLightStatus
	{
		NIGHT,
		TWILIGHT_1,
		TWILIGHT_2,
		TWILIGHT_3,
		TWILIGHT_4,
		DAY,
		UNKNOWN,
		INVALID
	}
}