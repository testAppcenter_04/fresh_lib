﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum DeliveryMode
	{
		PROMPT,
		DESTINATION,
		QUEUE
	}
}
