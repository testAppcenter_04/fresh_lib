﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum KeypressMode
	{
		SINGLE_KEYPRESS,
		QUEUE_KEYPRESSES,
		RESEND_CURRENT_ENTRY
	}
}