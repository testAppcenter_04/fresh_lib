﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum VehicleDataEventStatus
	{
		NO_EVENT,
		NO,
		YES,
		NOT_SUPPORTED,
		FAULT
	}
}
