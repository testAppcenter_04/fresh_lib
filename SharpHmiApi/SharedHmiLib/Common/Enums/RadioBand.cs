﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum RadioBand
	{
		AM,
		FM,
        XM
	}
}
