﻿using System;
namespace HmiApiLib.Common.Enums
{
    public enum FuelType
    {
        GASOLINE,
        DIESEL,
        CNG,
        LPG,
        HYDROGEN,
        BATTERY
    }
}
