﻿using System;
namespace HmiApiLib.Common.Enums
{
    public enum VideoStreamingProtocol
	{
        RAW,
        RTP,
        RTSP,
        RTMP,
        WEBM
	}
}
