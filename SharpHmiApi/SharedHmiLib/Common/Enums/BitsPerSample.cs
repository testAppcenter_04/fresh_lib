﻿using System;
using System.Runtime.Serialization;

namespace HmiApiLib.Common.Enums
{
	public enum BitsPerSample
	{
		[EnumMember(Value = "8_BIT")]
		RATE_8_BIT,
		[EnumMember(Value = "16_BIT")]
		RATE_16_BIT
	}
}