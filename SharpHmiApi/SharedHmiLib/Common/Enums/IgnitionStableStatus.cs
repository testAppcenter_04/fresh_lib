﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum IgnitionStableStatus
	{
		IGNITION_SWITCH_NOT_STABLE,
		IGNITION_SWITCH_STABLE,
		MISSING_FROM_TRANSMITTER
	}
}