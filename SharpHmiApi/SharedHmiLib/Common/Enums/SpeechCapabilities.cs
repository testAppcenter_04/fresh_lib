﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum SpeechCapabilities
	{
		TEXT,
		SAPI_PHONEMES,
		LHPLUS_PHONEMES,
		PRE_RECORDED,
		SILENCE,
        FILE
	}
}
