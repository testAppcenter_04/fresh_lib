﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum ModuleType
	{
		CLIMATE,
		RADIO,
        SEAT,
        AUDIO,
        LIGHT,
        HMI_SETTINGS
	}
}
