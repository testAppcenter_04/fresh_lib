﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum CarModeStatus
	{
		NORMAL,
		FACTORY,
		TRANSPORT,
		CRASH
	}
}