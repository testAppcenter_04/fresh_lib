﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum VentilationMode
	{
		UPPER,
		LOWER,
        BOTH,
        NONE
	}
}
