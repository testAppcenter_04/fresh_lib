﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum SystemContext
	{
		MAIN,
		VRSESSION,
		MENU,
		HMI_OBSCURED,
		ALERT
	}
}
