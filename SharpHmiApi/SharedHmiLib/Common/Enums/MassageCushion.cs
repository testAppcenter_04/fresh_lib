﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum MassageCushion
	{
		TOP_LUMBAR,
		MIDDLE_LUMBAR,
        BOTTOM_LUMBAR,
        BACK_BOLSTERS,
        SEAT_BOLSTERS
	}
}
