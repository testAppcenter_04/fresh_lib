﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum VehicleDataStatus
	{
		NO_DATA_EXISTS,
		OFF,
		ON
	}
}
