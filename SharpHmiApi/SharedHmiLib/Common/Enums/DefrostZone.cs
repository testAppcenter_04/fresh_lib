﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum DefrostZone
	{
		FRONT,
		REAR,
        ALL,
        NONE
	}
}
