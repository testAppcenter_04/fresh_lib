﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum TransportType
	{
		BLUETOOTH,
		USB_IOS,
		USB_AOA,
		WIFI
	}
}
