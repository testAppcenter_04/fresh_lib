﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum KeyboardLayout
	{
		QWERTY,
		QWERTZ,
		AZERTY
	}
}
