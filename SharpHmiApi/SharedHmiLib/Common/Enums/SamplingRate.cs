﻿using System;
using System.Runtime.Serialization;

namespace HmiApiLib.Common.Enums
{
	public enum SamplingRate
	{
		[EnumMember(Value = "8KHZ")]
		RATE_8KHZ,
		[EnumMember(Value = "16KHZ")]
		RATE_16KHZ,
		[EnumMember(Value = "22KHZ")]
		RATE_22KHZ,
		[EnumMember(Value = "44KHZ")]
		RATE_44KHZ
	}
}