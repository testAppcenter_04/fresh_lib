﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum UpdateResult
	{
		UP_TO_DATE,
		UPDATING,
		UPDATE_NEEDED
	}
}
